<tags data-value="Monitor,Views,Live,Health,Graph"></tags>

View All Monitor Views
=====
					
[Done this? Next step](#next step)

___

##Information

The [Monitor] View is where all your monitoring services are shown. 

You can filter, sort, show, edit, delete, Customize the Health Graph, and [Add or manage Monitor View].

___

##Instructions

### Overview

This is the overview of all the Monitor Views.

![Overview][1]

### Health Graph

The Health Graph shows how many, and the share of the monitoring services which is ok, warning and error in a circle diagram.

Clicking a share will show only the **Monitor Views** within itself.



![Health Graph][2]

####Customize Health Graph

The Health Graph is customizable and lets you choose if you want a certain **Monitor Views** to be shown.

All **Monitor Views** are shown by default.

![Customize Health Graph][8]

###Action

By clicking show under Action or the name of a **Monitor View**, Integration Manager will display it.

Edit will take you to the edit page, more information is found in [Add or manage Monitor View]. 

![Actions][3]

####Confirm Deletion

If you click delete, a pop up will be shown to confirm the deletion.

![Confirm Deletion][9]


###Actions on Sources

More information about [Actions] on Sources. 

![Windows Services][10]




<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/af151dcde04bf9487651a950cb9522f1425dbe3b/Media/Documentation%20Pictures/A26.%20Monitor.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/af151dcde04bf9487651a950cb9522f1425dbe3b/Media/Documentation%20Pictures/A26.2%20Health%20Graph.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/af151dcde04bf9487651a950cb9522f1425dbe3b/Media/Documentation%20Pictures/A26.3%20Monitor%20Actions%27.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/48dc04c3f0ea9b0dde3ed1696e05c66fa3e42c17/Media/Documentation%20Pictures/A27.%20Historic%20Events.png

[5]:https://bytebucket.org/integrationsoftware/products-documentation/raw/48dc04c3f0ea9b0dde3ed1696e05c66fa3e42c17/Media/Documentation%20Pictures/A27.%20Live%20Overview.png
[6]:https://bytebucket.org/integrationsoftware/products-documentation/raw/663df5fd5a6867c63dc3eee768e508077a636e3c/Media/Documentation%20Pictures/A27.3%20Single%20Monitor.png
[7]:https://bytebucket.org/integrationsoftware/products-documentation/raw/c90d29249fae13f74c1f8d52f08a395e914eef97/Media/Documentation%20Pictures/A27.4%20Customize%20Historic%20Events.png
[8]:https://bytebucket.org/integrationsoftware/products-documentation/raw/5fbaa600c1ddb78852ee514202889e1bec93a552/Media/Documentation%20Pictures/A27.5%20Customize%20Health%20Graph.png

[9]:https://bytebucket.org/integrationsoftware/products-documentation/raw/3775431376bb4838929261d320d473ba99b70659/Media/Documentation%20Pictures/A25.2%20Confirm%20Deletion.png
[10]:https://bytebucket.org/integrationsoftware/products-documentation/raw/637a7004a599b79f550d0ff686a258ac5dc1bcd9/Media/Documentation%20Pictures/C15.3%20Some%20Actions.png

[Actions]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/Actions/Actions.md?at=master
[Monitor]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master
[Add or manage Monitor View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master


___

###Next step
####Related
