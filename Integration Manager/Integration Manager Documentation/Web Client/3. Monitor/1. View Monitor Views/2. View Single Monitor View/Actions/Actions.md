<tags data-value="Actions,BizTalk,Windows,Services,Resource"></tags>

Actions
=====
					
[Done this? Next step](#next step)

___

##Information

On this page, Actions is the available operations for a [Resource] in a [Monitor View].

The Actions available depends if it's a [BizTalk] or [Windows Service] Resource.

More information about [View Single Monitor View].

More information about [Monitor Agents].

###BizTalk

For example, some of the available actions for BizTalk Resources:

![BizTalk Actions][3]

![List Suspended Instances][1]

###Windows Services

Some of the available actions for Windows Services Resources:

![Windows Services Actions][2]







<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/0b723d16c0f518560b8c6924a055b03cb01f4c72/Media/Documentation%20Pictures/C16.%20Action%20List%20suspended%20instances.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/637a7004a599b79f550d0ff686a258ac5dc1bcd9/Media/Documentation%20Pictures/C15.3%20Some%20Actions.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/0b723d16c0f518560b8c6924a055b03cb01f4c72/Media/Documentation%20Pictures/C15.%20All%20Actions.png

[Monitor Agents]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Monitor View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master
[Add or manage Monitor View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20Monitor%20View/Add%20Monitor%20View.md?at=master
[View Single Monitor View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/View%20Single%20Monitor%20View.md?at=master
[BizTalk]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Monitor%20Agents/BizTalk/BizTalk.md?at=master
[Windows Service]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Monitor%20Agents/Windows%20Services/Windows%20Services.md?at=master
[Resource]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
___

###Next step
####Related
