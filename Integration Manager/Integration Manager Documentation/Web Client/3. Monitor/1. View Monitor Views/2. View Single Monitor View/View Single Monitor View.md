<tags data-value="Monitor,Views,Live,Health,Graph"></tags>

View Single Monitor View
=====
					
[Done this? Next step](#next step)

___

##Information

The [Monitor] View is where all your monitoring services are shown. 

You can filter, sort, show, edit, delete, Customize the Health Graph, and [Edit Monitor View].

___

##Instructions


###View Single Monitor View

A single **Monitor View** shows a "Live Overview", the "Historic events", and the services it consists of.

![Single Monitor][6]

### Live Overview

The "Live Overview" is similar to the health graph but in shows single services within the **Monitor View** instead.

Clicking a share will show only the services within the share.


![Live View][5]

### Historic Events

Historic events shows the number of events within the chosen time span.

![Historic Events][4]

Under Customize you will find some alternatives of time spans:


|Show Last	|	
|---|
|7 days	|	
|4 weeks	|
|3 months	|	
|6 months	|	
|12 months	|	


![Customize][7]

###Monitor Agents



[Monitor Agents]



<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/0b723d16c0f518560b8c6924a055b03cb01f4c72/Media/Documentation%20Pictures/C15.%20All%20Actions.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/0b723d16c0f518560b8c6924a055b03cb01f4c72/Media/Documentation%20Pictures/C15.2%20No%20Actions.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/0b723d16c0f518560b8c6924a055b03cb01f4c72/Media/Documentation%20Pictures/C15.3%20Some%20Actions.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/48dc04c3f0ea9b0dde3ed1696e05c66fa3e42c17/Media/Documentation%20Pictures/A27.%20Historic%20Events.png

[5]:https://bytebucket.org/integrationsoftware/products-documentation/raw/48dc04c3f0ea9b0dde3ed1696e05c66fa3e42c17/Media/Documentation%20Pictures/A27.%20Live%20Overview.png
[6]:https://bytebucket.org/integrationsoftware/products-documentation/raw/663df5fd5a6867c63dc3eee768e508077a636e3c/Media/Documentation%20Pictures/A27.3%20Single%20Monitor.png
[7]:https://bytebucket.org/integrationsoftware/products-documentation/raw/c90d29249fae13f74c1f8d52f08a395e914eef97/Media/Documentation%20Pictures/A27.4%20Customize%20Historic%20Events.png

[8]:https://bytebucket.org/integrationsoftware/products-documentation/raw/6bb6f4873ed15e9ef819c3eae6ca72655a900f70/Media/Documentation%20Pictures/C15.5%20Details.png

[Monitor]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master
[Edit Monitor View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20Monitor%20View/Add%20Monitor%20View.md?at=master
[Monitor Agents]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/Monitor%20Agents/Monitor%20Agents.md?at=master

___

###Next step
####Related
