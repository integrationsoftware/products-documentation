<tags data-value="Create,Add,Monitor,View"></tags>

Link to Add or manage Monitor View
=====
					
[Done this? Next step](#next step)

___

[Add or manage Monitor View]

<!--References -->
[Add or manage Monitor View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
___

###Next step
####Related
