<tags data-value="Monitor,Sources,Live,View,Events"></tags>

Monitor
=====
					
[Done this? Next step](#next step)

___

##Information

The Monitor is where you see if your sources are up and running, or if there's errors.

You can [Add or manage Monitor Views] consisting of several different sources and watch them as both "Live View" and "Historic events".

More information about the overview functions: [View All Monitor Views].

This the monitor overview.

![Monitor][1]



<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/af151dcde04bf9487651a950cb9522f1425dbe3b/Media/Documentation%20Pictures/A26.%20Monitor.png
[View All Monitor Views]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/3.%20Monitor/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
___

###Next step
####Related
