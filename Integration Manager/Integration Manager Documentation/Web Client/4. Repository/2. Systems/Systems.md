<tags data-value="System,Services,End Points"></tags>

Systems
=====
					
[Done this? Next step](#next step)

___

##Information

A System consists of [Services] and [End Points].

___

The hierarchy of system integration:

|System Integration Order	|
|---|
|Integration	|
|System	|
|Service	|
|End Points	|
|Message Type	|

|Extra|
|---|
|Custom Field	|

<!--References -->

[Services]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[End Points]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master


___

###Next step
####Related
