<tags data-value="Add,Systems,Services,End Points"></tags>

Add or manage Systems
=====
					
[Done this? Next step](#next step)

___

##Information

A [System] consists of [Services] and [End Points].

Although, **Systems** are rarely used since **Services** and **End Points** works fine themselves.

##Instructions

A Name is required to create the **System**.

Adding a Description and a Website is optional. 

You will be able to search for the **System** by the text in the Description.

![Name, Description, and Website][1]

### Custom Field
You can also add a [Custom Field] to provide documentation to your **System**.


<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/027396318338def41925ba11e052356ee8b109c1/Media/Documentation%20Pictures/A33.%20Name%2C%20Description%2C%20And%20Website%2C%20Systems.png

[System]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
[Custom Field]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master
[Services]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[End Points]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master


___

###Next step
####Related
