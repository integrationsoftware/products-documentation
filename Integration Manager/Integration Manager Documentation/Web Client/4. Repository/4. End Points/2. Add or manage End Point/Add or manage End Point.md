<tags data-value="Add,End Point,URI,Type,Direction"></tags>

Add or manage End Point
=====
					
[Done this? Next step](#next step)

___

##Information

An [End Point] is the target where a message is heading, it could for example be a port.

##Instructions

A Name, URI, End Point Type and Direction is required to create the **End Point**.

Adding a Description and a Website is optional. 

You will be able to search for the **End Point** by the text in the Description.

![Name, Description, And Website][1]

####URI

URI means Uniform Resource Identifier and is the link to the End Point.

####End Point Types

The Integration Manager default End Point Types to choose between is: 

|	|
|---|
|AppFabric	|
|AppFabric (Azure)	|
|BizTalk - Dynamic Send	|
|BizTalk - Receive, Biztalk - Send	|
|MSMQ	|
|Unknown	|
|WCF	|
|WMQ	|



![End Point Type][4]

![End Point Type 2][5]

#### Directions

The directions to choose between is Send, Recieve and Unknown.

![Directions][3]




You can [Add or manage a Custom Field to an End Point]

![Custom Fields][2]




<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/3d1d2679c0bd6059e06078866c330a0237bbda4a/Media/Documentation%20Pictures/A41.%20Add%20or%20manage%20End%20Point.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/3d1d2679c0bd6059e06078866c330a0237bbda4a/Media/Documentation%20Pictures/A41.2%20Custom%20Fields.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/3d1d2679c0bd6059e06078866c330a0237bbda4a/Media/Documentation%20Pictures/A39.2%20Directions.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/5927fa1a63b267d6ad743518658eff03f448d047/Media/Documentation%20Pictures/A44.%20End%20Point%20Type.png
[5]:https://bytebucket.org/integrationsoftware/products-documentation/raw/5927fa1a63b267d6ad743518658eff03f448d047/Media/Documentation%20Pictures/A44.2%20End%20Point%20Type%202.png

[End Point]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master

[Add or manage a Custom Field to an End Point]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/1.%20Integrations/2.%20Add%20or%20manage%20Integration/2.%20Add%20Custom%20Fields%20to%20artifact/Add%20Custom%20Fields%20to%20artifact.md?at=master
___

###Next step
####Related
