<tags data-value="View, All,End Points,Direction"></tags>

View All End Points
=====
					
[Done this? Next step](#next step)

___

##Information



The [End Points] overview shows all the **End Points** where you can filter by both characters and direction, edit, delete, [Include Deleted], and [Add or manage End Point].

The directions to choose between is Send, Recieve and Unknown.

![Directions][2]

This is the **End Points** overview.

![End Points][1]



<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/507340d4a88896693e5cada8479d5c1c4bbf2979/Media/Documentation%20Pictures/A39.%20End%20Points.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/3d1d2679c0bd6059e06078866c330a0237bbda4a/Media/Documentation%20Pictures/A39.2%20Directions.png

[Include Deleted]: https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/8.%20Include%20Deleted/Include%20Deleted.md?at=master
[Add or manage End Point]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/2.%20Add%20or%20manage%20End%20Point/Add%20or%20manage%20End%20Point.md?at=master
[End Points]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
___

###Next step
####Related
