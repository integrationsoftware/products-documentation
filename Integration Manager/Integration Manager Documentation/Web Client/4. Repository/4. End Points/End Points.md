<tags data-value="End Point,System,Integration"></tags>

End Points
=====
					
[Done this? Next step](#next step)

___

##Information

An **End Point** is the target where a message is heading, it could for example be a port.

___

The hierarchy of system integration:


|System Integration Order	|
|---|
|Integration	|
|System	|
|Service	|
|End Points	|
|Message Type	|

|Extra|
|---|
|Custom Field	|

[How to add or manage an End Point].

<!--References -->
[How to add or manage an End Point]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/2.%20Add%20or%20manage%20End%20Point/Add%20or%20manage%20End%20Point.md?at=master
___

###Next step
####Related
