<tags data-value="View,Message Type,Include Deleted"></tags>

View All Message Types
=====
					
[Done this? Next step](#next step)

___

##Information

The [Message Type] overview shows all the **Message Types** where you can filter by characters, edit, delete, [Include Deleted], and [Add or manage Message Type].

This is the **Message Types** overview.


![Message Type][1]


<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/507340d4a88896693e5cada8479d5c1c4bbf2979/Media/Documentation%20Pictures/A40.%20Message%20Types.png

[Message Type]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
[Add or manage Message Type]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/2.%20Add%20or%20manage%20Message%20Type/Add%20or%20manage%20Message%20Type.md?at=master
[Include Deleted]: https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/8.%20Include%20Deleted/Include%20Deleted.md?at=master


___

###Next step
####Related
