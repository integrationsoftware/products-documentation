<tags data-value="Message Type, System Integration,Log"></tags>

Message Types
=====
					
[Done this? Next step](#next step)

___

##Information

A Message Type is the type of message or event you want to log.

You may also decide for how long to keep messages.

Fewer logged messages may make searches faster.
___

The hierarchy of system integration:


|System Integration Order	|
|---|
|Integration	|
|System	|
|Service	|
|End Points	|
|Message Type	|

|Extra|
|---|
|Custom Field	|

[How to add or manage a Message Type].

<!--References -->
[How to add or manage a Message Type]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/2.%20Add%20or%20manage%20Message%20Type/Add%20or%20manage%20Message%20Type.md?at=master
___

###Next step
####Related
