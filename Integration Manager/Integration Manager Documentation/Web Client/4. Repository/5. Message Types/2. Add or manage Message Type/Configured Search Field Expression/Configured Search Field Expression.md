<tags data-value="Add, Message Type"></tags>

Configure Search Field 
=====
					
[Done this? Next step](#next step)

___

##Information

Search Field Expressions are limitations of a Search Field and are edited in [Configure Search Field Expression].



<!--References -->
[Configure Search Field Expression]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/2.%20Add%20Search%20Field/Configure%20Search%20Field%20Expression/Configure%20Search%20Field%20Expression.md?at=master
___

###Next step
####Related
