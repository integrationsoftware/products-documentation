<tags data-value="Add,Message Type,Custom Field,Search Field"></tags>

Add or manage Message Type
=====
					
[Done this? Next step](#next step)

___

##Information

A [Message Type] is the type of message or event you want to log.

##Instructions

A Name is required to create the **Message Type**.

Adding a Description and a Website is optional. 

You will be able to search for the **Message Type** by the text in the Description.

![Name, Description, and Website][1]

###Days to Keep Messages

"Days to Keep Messages" and "Days to Keep Events" means how long to save Message or Events tagged with this Message Type.

If set to 0 (zero), Messages or Events will not be removed

If too many Messages or Events are logged, searching for data may be significantly slower than with less data.



###Custom Fields

You can [Add or manage a Custom Field to a Message Type] to add additional information. 

![Custom Fields and Search Field Expression][3]

###Search Field Expressions

When a **Message Type** is connected to a [Search Field] Expression, it will appear on the edit page of the **Message Type**.

Search Field Expressions connects to Message Types in [Add or manage Search Field] or on the edit page.

![Custom Fields and Search Field Expression 2][2]

You can edit a Search Field by clicking its name when editing a **Message Type**.

![Edit Search Field][4]



<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/edeab805b3264f301a31a244169d54f6bf1d56ce/Media/Documentation%20Pictures/A45.%20Name%2C%20Description%2C%20and%20Website.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/edeab805b3264f301a31a244169d54f6bf1d56ce/Media/Documentation%20Pictures/A45.2%20Custom%20Fields%20and%20Search%20Field.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/edeab805b3264f301a31a244169d54f6bf1d56ce/Media/Documentation%20Pictures/A45.3%20Custom%20Fields%20and%20Search%20Fields%202.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/c10d025f8981035bc0fb839dd33832f289926ca4/Media/Documentation%20Pictures/A45.4%20Search%20Field%20Edit.png

[Message Type]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
[Add or manage Search Field]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/2.%20Add%20Search%20Field/Add%20Search%20Field.md?at=master
[Search Field]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/Search%20Fields.md?at=master
[Add or manage a Custom Field to a Message Type]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/1.%20Integrations/2.%20Add%20or%20manage%20Integration/2.%20Add%20Custom%20Fields%20to%20artifact/Add%20Custom%20Fields%20to%20artifact.md?at=master

___


###Next step
####Related
