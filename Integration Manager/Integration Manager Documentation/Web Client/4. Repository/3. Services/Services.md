<tags data-value="Service,Message Types,End Points"></tags>

Services
=====
					
[Done this? Next step](#next step)

___

##Information

A **Service** is the connection between [Message Types] and [End Points].

**Services** is useful when you have more than one **Message Type** to one **End Point** or the other way around.
___

The hierarchy of system integration:

|System Integration Order	|
|---|
|Integration	|
|System	|
|Service	|
|End Points	|
|Message Type	|

|Extra|
|---|
|Custom Field	|

[How to add or manage a Service].

<!--References -->

[End Points]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
[Message Types]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
[How to add or manage a Service]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/Add%20or%20manage%20Service.md?at=master

___

###Next step
####Related
