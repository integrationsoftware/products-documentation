<tags data-value="Connect,Message,Type,Service"></tags>

Connect Message Type
=====
					
[Done this? Next step](#next step)

___

##Information

One or more [Message Types] can be added to a [Service].

You have the options to Add, Remove, and Filter the result of one or more **Message Types**.

![Connect Message Type][1]










<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/29ebcc66f2eec9f18a64c484e6405af77da78ba8/Media/Documentation%20Pictures/A43.%20Connect%20Message%20Type.png

[Service]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[Message Types]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
___

###Next step
####Related
