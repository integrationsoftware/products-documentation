<tags data-value="Add,Systems,Services,End Points"></tags>

-Link to Add Custom Field to artifact-
=====
					
[Done this? Next step](#next step)

___

[Add or manage Custom Field to artifact]

<!--References -->

[Add or manage Custom Field to artifact]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/1.%20Integrations/2.%20Add%20or%20manage%20Integration/2.%20Add%20Custom%20Fields%20to%20artifact/Add%20Custom%20Fields%20to%20artifact.md?at=master

___

###Next step
####Related
