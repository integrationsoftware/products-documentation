<tags data-value="Add,Service,End Points"></tags>

Connect End Points
=====
					
[Done this? Next step](#next step)

___

##Information

One or more [End Points] can be added to a [Service].

You have the options to Add, Remove, and Filter the result of one or more **End Points**.

![Connect End Points][1]






<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/3d1d2679c0bd6059e06078866c330a0237bbda4a/Media/Documentation%20Pictures/A41.3%20Connect%20End%20Points.png

[Service]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[End Points]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
___

###Next step
####Related
