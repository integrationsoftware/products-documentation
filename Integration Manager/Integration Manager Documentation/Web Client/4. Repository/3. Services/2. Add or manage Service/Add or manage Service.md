<tags data-value="Add,Service,Message Types,End Points,Custom Fields"></tags>

Add or manage Service
=====
					
[Done this? Next step](#next step)

___

##Information

A [Service] can be connected to [Message Types], [End Points], and [Custom Fields]. 

##Instructions

A Name is required to create the **Service**.

Adding a Description and a Website is optional. 

You will be able to search for the **Service** by the text in the Description.

You can select a [System] to associate it with the **Service**.

![Add Service][2]

###Connections

There are several possible connections to a **Service**.

![Extras][3]

### Message Types

How to connect a **Message Type** is found in [Connect Message Type].

### End Points

How to connect an **End Point** is found in [Connect End Points].

### Custom Fields

How to connect a **Custom Field** is found in [Add or manage Custom Field to artifact]








<!--References -->
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/78a5007e93d10cca634b63ac68e495ccb9c5b4b0/Media/Documentation%20Pictures/A38.%20Add%20or%20manage%20Service.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/78a5007e93d10cca634b63ac68e495ccb9c5b4b0/Media/Documentation%20Pictures/A38.2%20Add%20or%20manage%20Service%20Extras.png

[System]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
[Connect Message Type]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
[Connect End Points]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/2.%20Connect%20End%20Points/Connect%20End%20Points.md?at=master
[Add or manage Custom Field to artifact]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/1.%20Integrations/2.%20Add%20or%20manage%20Integration/2.%20Add%20Custom%20Fields%20to%20artifact/Add%20Custom%20Fields%20to%20artifact.md?at=master
[Service]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[Custom Fields]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master
[End Points]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
[Message Types]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
___

###Next step
####Related
