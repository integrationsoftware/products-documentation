<tags data-value="Add,Custom Field,Value,Type"></tags>

Add or manage Custom Field
=====
					
[Done this? Next step](#next step)

___

##Information

A [Custom Field] is useful when you want to add additional information about an artifact.

##Instructions

![Custom Field][1]

### Field Settings

#### Single Value

Check the box if only a single value will be selected. If unchecked, multiple values can be selected.

#### Custom Field Type 

The **Custom Field** Types to choose between is File and Text.


![Custom Field Type][2]

After a **Custom Field** Type is chosen, a Value may be added by clicking Edit.

![Values][3] 


<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/24b021fa533e72c53c3d3e8f1fd48eb0063651dd/Media/Documentation%20Pictures/A48.%20Add%20Custom%20Field.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/38a417d36639e8a48e9701e392685260f8f763b3/Media/Documentation%20Pictures/A48.2%20Custom%20Field%20Type.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/84d5bc0997548848af0c9f9ba8f9494378980796/Media/Documentation%20Pictures/A48.3%20Values.png

[Custom Field]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master

___

###Next step
####Related
