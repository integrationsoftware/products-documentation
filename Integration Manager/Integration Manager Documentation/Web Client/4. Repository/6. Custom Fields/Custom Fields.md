<tags data-value="Custom Field,System,Integration,File,Text"></tags>

Custom Field
=====
					
[Done this? Next step](#next step)

___

##Information

A Custom Field is an extra piece of information added to an artifact such as an Integration or a System.

It could be a file or a piece of text.

It's useful when you want some extra information to be available to your colleagues about the artifact.

___

The hierarchy of system integration:


|System Integration Order	|
|---|
|Integration	|
|System	|
|Service	|
|End Points	|
|Message Type	|

|Extra|
|---|
|Custom Field	|

[How to add or manage Custom Fields].

<!--References -->

[How to add or manage Custom Fields]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/2.%20Add%20or%20manage%20Custom%20Fields/Add%20or%20manage%20Custom%20Fields.md?at=master
___

###Next step
####Related
