<tags data-value="View,All,Custom Fields,Type"></tags>

View All Custom Fields
=====
					
[Done this? Next step](#next step)

___

##Information

The [Custom Fields] overview shows all the **Custom Fields** where you can filter by both characters and Custom Field Type, edit, delete, [Include Deleted], and [Add or manage Custom Field].

This is the **Custom Field** overview.

![Custom Fields overview][1]

###Custom Field Type

The Custom Field Types to choose between is File and Text.

![Filter][2]


<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/641329e5ce614545adaccac38826e0085a0eb516/Media/Documentation%20Pictures/A46.%20Custom%20Fields.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/641329e5ce614545adaccac38826e0085a0eb516/Media/Documentation%20Pictures/A46.2%20Filter.png

[Include Deleted]: https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/8.%20Include%20Deleted/Include%20Deleted.md?at=master
[Add or manage Custom Field]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/2.%20Add%20or%20manage%20Custom%20Fields/Add%20or%20manage%20Custom%20Fields.md?at=master
[Custom Fields]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master
___

###Next step
####Related
