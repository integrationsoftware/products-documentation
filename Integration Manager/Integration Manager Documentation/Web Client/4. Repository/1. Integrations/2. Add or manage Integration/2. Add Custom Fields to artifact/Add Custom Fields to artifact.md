<tags data-value="Add,Custom Fields,Artifact,Connect"></tags>

Add Custom Fields to artifact
=====
					
[Done this? Next step](#next step)

___
##Information

You can add a [Custom Field] to an artifact such as an **Integration** to add additional information.

Choose a **Custom Field** and one or more Values to Add it.



In this example, **Custom Fields** are added to an **Integration**:

![Connect Custom Fields][1]

You can use Action on the chosen **Custom Field** to edit or delete it.

![Action][2]



![Add Custom Field to Integration][4]

![Values][5]


You can delete the **Custom Field** in the edit artifact view as well.

![Delete Custom Field][3]

<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/7e17c32f480aca38ffae57aeb675118ff25bcbd5/Media/Documentation%20Pictures/A42.%20Connect%20Custom%20Fields.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/498fbb3e2fbdd80646316fa405a23532bb12a739/Media/Documentation%20Pictures/A30.3%20Custom%20Fields%20Actions.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/b672f17df1a170692f3ea0431e692bfa8b485b75/Media/Documentation%20Pictures/A42.3%20Delete%20Custom%20Field.png

[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/498fbb3e2fbdd80646316fa405a23532bb12a739/Media/Documentation%20Pictures/A30%20Custom%20Fields.png
[5]:https://bytebucket.org/integrationsoftware/products-documentation/raw/498fbb3e2fbdd80646316fa405a23532bb12a739/Media/Documentation%20Pictures/A30.2%20Custom%20Field%20Vaues.png


[Custom Field]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master

___

###Next step
####Related
