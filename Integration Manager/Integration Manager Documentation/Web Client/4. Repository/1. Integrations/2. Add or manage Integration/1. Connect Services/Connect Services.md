<tags data-value="Connect,Service,Integration"></tags>

Connect Services
=====
					
[Done this? Next step](#next step)

___
##Information



You can connect a [Service] to an [Integration].

You have the options to Add, Remove, and Filter the result of one or more **Services**.

![Connect Services to Integration][1]


<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/498fbb3e2fbdd80646316fa405a23532bb12a739/Media/Documentation%20Pictures/A29.%20Services.png
[Service]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[Integration]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/1.%20Integrations/Integrations.md?at=master


___

###Next step
####Related
