<tags data-value="Add,Integration,System,Service,Custom Field"></tags>

Add or manage Integration
=====
					
[Done this? Next step](#next step)

___
##Information

[Integration] is the linking between systems.

[Services] and [Custom Fields] can be connected to the **Integration**, how it's done is found on [Add or manage Service] and [Add or manage Custom Field].
___

### Name, Description and Website

A Name is required to create the **Integration**.

Adding a Description and a Website is optional. 

You will be able to search for the **Integration** by the text in the Description.

![Name, Description and Website][1]

###Services and Custom Fields Options

An example of **Services** and **Custom Fields** connected to an **Integration**.



![Services and custom Fields][2]



<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/498fbb3e2fbdd80646316fa405a23532bb12a739/Media/Documentation%20Pictures/A28.%20Name%2C%20Description%2C%20and%20Website.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/498fbb3e2fbdd80646316fa405a23532bb12a739/Media/Documentation%20Pictures/A28.%20Services%20and%20Custom%20Fields.png


[Services]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[Custom Fields]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master
[Integration]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/1.%20Integrations/Integrations.md?at=master
[Add or manage Service]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/Add%20or%20manage%20Service.md?at=master
[Add or manage Custom Field]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/2.%20Add%20or%20manage%20Custom%20Fields/Add%20or%20manage%20Custom%20Fields.md?at=master

___

###Next step
####Related
