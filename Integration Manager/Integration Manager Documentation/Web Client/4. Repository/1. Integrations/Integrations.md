<tags data-value="Integration,Service,Custom Field,System"></tags>

Integrations
=====
					
[Done this? Next step](#next step)

___

##Information
An integration is the linking between two or more systems.

You can add **Services** to connect the entities part of your system integration and provide documentation by **Custom Fields**.

___

The hierarchy of system integration:

|System Integration Order	|
|---|
|Integration	|
|System	|
|Service	|
|End Points	|
|Message Type	|

|Extra|
|---|
|Custom Field	|

[How to add or manage an Integration].

<!--References -->
[How to add or manage an Integration]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/1.%20Integrations/2.%20Add%20or%20manage%20Integration/Add%20or%20manage%20Integration.md?at=master
___

###Next step
####Related
