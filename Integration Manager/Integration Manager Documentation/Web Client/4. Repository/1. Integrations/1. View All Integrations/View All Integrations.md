<tags data-value="Integration,Service,Custom Field,System"></tags>

View All Integrations
=====
					
[Done this? Next step](#next step)

___

##Information

The [Integration] overview shows all the **Integrations** where you can filter, edit, delete, show information by dropdown, [Include Deleted], and [Add or manage Integration].

___

##Instructions

This is the **Integration** overview.

![Integrations][1]

###Show information

The dropdown button will show information about the **Integration** including [Services] and [Custom Fields].

####Dropdown Button

![Button][2]

####Information

![Dropdown][3]




<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/6246685a3e446af98ef8e102547612361a34e887/Media/Documentation%20Pictures/A31.%20Integrations.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/6246685a3e446af98ef8e102547612361a34e887/Media/Documentation%20Pictures/A12.%20Button.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/ed42c8a86849cf3f26bf6b46956bd2240fa116ba/Media/Documentation%20Pictures/A32.%20Integration%20Dropdown.png

[Integration]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/1.%20Integrations/Integrations.md?at=master
[Services]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[Custom Fields]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master
[Include Deleted]: https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/8.%20Include%20Deleted/Include%20Deleted.md?at=master
[Add or manage Integration]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/1.%20Integrations/2.%20Add%20or%20manage%20Integration/Add%20or%20manage%20Integration.md?at=master


___

###Next step
####Related
