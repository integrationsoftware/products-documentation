<tags data-value="Repository,Create,System,Integration"></tags>

Repository
=====
					
[Done this? Next step](#next step)

___

##Information

The Repository is where you will find the [Integrations], [Systems], [Services], [End Points], [Message Types], and [Custom Fields] to create your System Integration.

		Tip: Integrations are created from the bottom up, start by Message Types.

![Repository][1]

<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/d69fa4c0d121276a1dce58d7a99a5012a9933954/Media/Documentation%20Pictures/A92.%20Repository.png

[Integrations]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/1.%20Integrations/Integrations.md?at=master
[Systems]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
[Services]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[End Points]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
[Message Types]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
[Custom Fields]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master


___

###Next step
####Related
