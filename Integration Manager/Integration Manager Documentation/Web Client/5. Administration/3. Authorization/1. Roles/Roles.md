<tags data-value="Roles,Users,Authority"></tags>

Roles
=====
					
[Done this? Next step](#next step)

___

##Information

Roles are groups with certain authorities.

[Users] can be assigned to Roles and be given new permissions.

For example, you can allow certain Roles to have access to a [Log View].

[How to add or manage a Role].

<!--References -->

[Users]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/Log.md?at=master
[How to add or manage a Role]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master
___

###Next step
####Related
