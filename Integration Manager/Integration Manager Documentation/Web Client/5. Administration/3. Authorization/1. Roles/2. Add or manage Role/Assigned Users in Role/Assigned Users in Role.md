<tags data-value="Assigned,Users,Role"></tags>

Assigned Users in Role
=====
					
[Done this? Next step](#next step)

___

##Information

One or more [Users] can be added to a [Role].

You have the options to Add, Remove, and Filter Users.

![Assign User to Role][1]

<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/fddb76d580c49da689ace9803e6a509b05497d86/Media/Documentation%20Pictures/A91.%20Assign%20Users%20to%20Role.png

[Role]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Users]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
___

###Next step
####Related
