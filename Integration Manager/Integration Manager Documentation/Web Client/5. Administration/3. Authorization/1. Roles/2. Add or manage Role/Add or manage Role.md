<tags data-value="Add,Role,Assign,Users"></tags>

Add or manage Role
=====
					
[Done this? Next step](#next step)

___

##Information

A [Role] is a group which gives [Users] within it new permissions in Integration Manager. 

##Instructions


A Name is required to create the Role.

Adding a Description is optional. 

![Add Role][1]

####Assign Users in Role

You have the option to [Assign Users to the Role].

![Assign Users in Role][2]



<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A69.%20Add%20Role.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/b9a86a76c90511f73a797875ca5539a472a10609/Media/Documentation%20Pictures/A69.%20Assign%20Users%20to%20Role.png

[Users]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Role]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Assign Users to the Role]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Assigned%20Users%20in%20Role/Assigned%20Users%20in%20Role.md?at=master
___

###Next step
####Related
