<tags data-value="Users,Roles,Account"></tags>

Users
=====
					
[Done this? Next step](#next step)

___

##Information


A User is an account on Integration Manager from which persons will access the platform.

[Roles] can be added to Users to give them extra permissions. 

[How to add or manage a User].

<!--References -->

[Roles]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[How to add or manage a User]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/2.%20Add%20or%20manage%20User/Add%20or%20manage%20User.md?at=master

___

###Next step
####Related
