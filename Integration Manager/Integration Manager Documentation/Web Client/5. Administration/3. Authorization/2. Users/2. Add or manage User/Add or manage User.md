<tags data-value="Add,User,Access,Mail,Roles"></tags>

Add or manage User
=====
					
[Done this? Next step](#next step)

___

##Information

A [User] is an account on Integration Manager from which persons will access the platform.

##Instructions

A User name is required to create the User.

Additional information can be added such as title and assignments.

An e-mail address can be added under Mail address.

![Add User][1]

You have the option to assign [Roles] to the User.

![Edit Roles][2]

<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A72.%20Add%20User.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/38bf41917b677bfead899affef8c4fee1f86a4ef/Media/Documentation%20Pictures/A72.%20Edit%20Roles.png

[User]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Roles]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master

___

###Next step
####Related
