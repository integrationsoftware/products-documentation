<tags data-value="Edit,Roles,User"></tags>

Edit Roles
=====
					
[Done this? Next step](#next step)

___

##Information

Choose which [Roles] should be given to the [User].

##Instructions

You can Add, Filter and Remove Roles.

![Edit Roles][1]

<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/2e213bc9df213136d2d315d380da2b191e2f36cc/Media/Documentation%20Pictures/A73.%20Edit%20Roles%20to%20Users.png
[User]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Roles]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master

___

###Next step
####Related
