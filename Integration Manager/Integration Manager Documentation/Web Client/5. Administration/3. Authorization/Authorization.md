<tags data-value="Authorization,Users,Roles"></tags>

Authorization
=====
					
[Done this? Next step](#next step)

___

##Information

Authorization is where you administrate [Users] and [Roles].

<!--References -->

[Users]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Roles]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master

___

###Next step
####Related
