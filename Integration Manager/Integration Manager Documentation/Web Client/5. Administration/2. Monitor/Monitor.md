<tags data-value="Monitor,View,Sources,Categories,Application"></tags>

Monitor
=====
					
[Done this? Next step](#next step)

___

##Information

From the **Monitor**, you have access to the [Sources], [Resources], [Categories], and [Applications] to create and configure a [Monitor View].

<!--References -->

[Sources]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Resources]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Categories]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master
[Applications]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master
[Monitor View]:https://bitbucket.org/integrationsoftware/products-documentation/src/07eaf2ec0d3b2df6484e2eea0f263cc72a529b3a/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
___

###Next step
####Related
