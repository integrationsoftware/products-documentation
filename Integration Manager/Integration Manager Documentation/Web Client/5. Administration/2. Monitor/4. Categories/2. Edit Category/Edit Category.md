<tags data-value="Edit,Category,Web,Site"></tags>

Edit Category
=====
					
[Done this? Next step](#next step)

___

##Information

A Web Site for additional information can be added while the other information is only displayed.


![Edit Resource][1]

<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/f45bc376028b43f52dd5a93d1ba5d4753b03b95b/Media/Documentation%20Pictures/A89.%20Edit%20Category.png
___

###Next step
####Related
