<tags data-value="View,All,Categories"></tags>

View All Categories
=====
					
[Done this? Next step](#next step)

___

##Information

The [Categories] overview shows all the **Categories** where you can filter by characters, edit, delete, and [Include Deleted].

This is the **Categories** overview.

![View All Categories][1]


<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A66.%20View%20All%20Categories.png

[Categories]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master
[Include Deleted]: https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/8.%20Include%20Deleted/Include%20Deleted.md?at=master

___

###Next step
####Related
