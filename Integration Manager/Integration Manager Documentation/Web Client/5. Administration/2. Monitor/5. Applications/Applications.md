<tags data-value="Applications,Source,Resource"></tags>

Applications
=====
					
[Done this? Next step](#next step)

___

##Information

An **Application** belongs to a [Source] and is a way to divide [Resources].

<!--References -->

[Source]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Resources]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master



___

###Next step
####Related
