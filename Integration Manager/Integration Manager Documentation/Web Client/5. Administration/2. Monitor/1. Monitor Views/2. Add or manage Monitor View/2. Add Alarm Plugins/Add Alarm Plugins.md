<tags data-value="Alarm,Plugin,Email,Errors"></tags>

Alarm Plugins
=====
					
[Done this? Next step](#next step)

___

##Information

You can add an [Alarm Plugin] to a Monitor View to send information when errors or warnings occur.

For example, the email plugin sends an email to notify about changes in certain logs.

##Instructions

You can Add, Filter and Remove Alarm Plugins.

![Alarm Plugins][1]

<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/d74454773b980a36145460a1307243f9c4418f62/Media/Documentation%20Pictures/A58.%20Alarm%20Plugins.png

[Alarm Plugin]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/6.%20Notifications/Alarm%20Plugins/Alarm%20Plugins.md?at=master
___

###Next step
####Related
