<tags data-value="Included,Monitoring,Resources,Sources"></tags>

Included Monitoring Resources
=====
					
[Done this? Next step](#next step)

___

##Information

Included Monitoring Resources is the [Resources] included in the [Monitor View].

##Instructions

You can Add, Remove and Filter based on [Source], [Application], [Category], and characters.

![Monitoring Resources][1]

####Filter on Source

![Filter on Source][2]

####Filter on Application

![Filter on Application][3]

####Filter on Category

![Filter on Category][4]



<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/6f89afe5736d1ca5e1c7b7ba4ade58bb9353f4db/Media/Documentation%20Pictures/A60.%20Included%20Monitoring%20Resources.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/6f89afe5736d1ca5e1c7b7ba4ade58bb9353f4db/Media/Documentation%20Pictures/A60.2%20Filter%20on%20Source.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/6f89afe5736d1ca5e1c7b7ba4ade58bb9353f4db/Media/Documentation%20Pictures/A60.3%20Filter%20on%20Application.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/6f89afe5736d1ca5e1c7b7ba4ade58bb9353f4db/Media/Documentation%20Pictures/A60.4%20Filter%20on%20Category.png

[Source]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Application]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master
[Category]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master
[Resources]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
___

###Next step
####Related
