<tags data-value="Included,Integrations,Monitor,View"></tags>

Included Integrations
=====
					
[Done this? Next step](#next step)

___

##Information

Choose which [Integrations] should be included in the [Monitor View].

##Instructions

You can Add, Filter and Remove **Integrations**

![Include Integrations][1]


<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/473df2f473492568686f5d93cadc15bf9833af8d/Media/Documentation%20Pictures/A62.%20Include%20Integrations.png

[Monitor View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Integrations]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/1.%20Integrations/Integrations.md?at=master
___

###Next step
####Related
