<tags data-value="Monitor,Sources,View"></tags>

Monitor Sources
=====
					
[Done this? Next step](#next step)

___

##Information

Select the [Sources] you want to monitor in the [Monitor View].

##Instructions


You can Add, Filter and Remove Sources.

![Monitor Sources][1]



<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/a5abfc7fc6fda292278383d12a9137828ff48560/Media/Documentation%20Pictures/A59.Monitor%20Sources.png

[Sources]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Monitor View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
___

###Next step
####Related
