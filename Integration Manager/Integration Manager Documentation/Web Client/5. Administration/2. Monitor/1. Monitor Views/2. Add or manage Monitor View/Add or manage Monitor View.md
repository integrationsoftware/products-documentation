<tags data-value="Create,Add,Monitor,View,History"></tags>

Add or manage Monitor View
=====
					
[Done this? Next step](#next step)

___

##Information

Add a [Monitor View] to keep track of Log events.

##Instructions

####Name, Description and Web Site

A Name is required to create the **Monitor View**.

Adding a Description and a Website is optional. 

You will be able to search for the **Monitor View** by the text in the Description.

![Add Monitor View][1]

####Show history of monitor events 

If history should be viewable in the view.

####Allow Actions

If actions on Resource should be allowed in the view. This will control the Resource listed by Monitor Agents.

###Edit

You can choose: [Allow These Roles], [Alarm Plugins], [Monitor Sources], [Included Monitoring Resources], [Excluded Monitoring Resources], and [Included Integrations].

![Monitor Extras][2]


<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/6aae7e2b946afaeb09f29d1b0f650843fa3a504a/Media/Documentation%20Pictures/A87.%20Add%20or%20manage%20Monitor%20View.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/6aae7e2b946afaeb09f29d1b0f650843fa3a504a/Media/Documentation%20Pictures/A87.%20Monitor%20Extras.png

[Allow These Roles]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/1.%20Allow%20These%20Roles/Allow%20These%20Roles.md?at=master
[Alarm Plugins]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/2.%20Add%20or%20manage%20Alarm%20Plugins/Add%20or%20manage%20Alarm%20Plugins.md?at=master
[Monitor Sources]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/3.%20Monitor%20Sources/Monitor%20Sources.md?at=master
[Included Monitoring Resources]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/4.%20Included%20Monitoring%20Resources/?at=master
[Excluded Monitoring Resources]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/4.%20Included%20Monitoring%20Resources/?at=master
[Included Integrations]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/6.%20Included%20Integrations/?at=master
[Monitor View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master

___

###Next step
####Related
