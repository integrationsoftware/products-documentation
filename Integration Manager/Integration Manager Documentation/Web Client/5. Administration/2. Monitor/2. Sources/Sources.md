<tags data-value="Sources,URL,Interval,Authentication,Key"></tags>

Sources
=====
					
[Done this? Next step](#next step)

___

##Information

A **Source** is a URL from which information is retrieved.

Information is retrieved from the source from a chosen interval. 

An Authentication Key may be chosen if the source is secured.

[How to  or manage a Source].

<!--References -->

[How to add or manage a Source]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
___

###Next step
####Related
