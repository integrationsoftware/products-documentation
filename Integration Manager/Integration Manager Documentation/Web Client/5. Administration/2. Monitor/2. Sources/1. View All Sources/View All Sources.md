<tags data-value="Log,Monitor"></tags>

View All Sources
=====
					
[Done this? Next step](#next step)

___

##Information

The [Sources] overview shows all the **Sources** where you can filter by characters, edit, delete, [Include Deleted], and [Add or manage Sources].

This is the **Sources** overview.


![View All Sources][1]

<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/0c3c440ad55af43400bdb7be2e402578f5558561/Media/Documentation%20Pictures/A63.%20View%20All%20Sources.png

[Sources]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Add or manage Sources]:https://bitbucket.org/integrationsoftware/products-documentation/src/0c3c440ad55af43400bdb7be2e402578f5558561/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Include Deleted]: https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/8.%20Include%20Deleted/Include%20Deleted.md?at=master

___

###Next step
####Related
