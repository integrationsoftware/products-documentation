<tags data-value="Add,Source,URL,Polling,Authentication"></tags>

Add or manage Source
=====
					
[Done this? Next step](#next step)

___

##Information

A [Source] is a URL from which information is retrieved.

##Instructions

A Name is required to create the **Source**.

Adding a Description and a Website is optional. 

You will be able to search for the **Source** by the text in the Description.

![Add Source][1]

####Service URL

Service URL is the address to the source.

####Polling Interval

Polling Interval is how often to poll data from the **Source** in seconds.

####Timeout

Check "Change default Timeout" if the **Source** should use a custom Timeout.

The default time is 120 seconds.

####Authentication Key

If the **Source** is secured, an Authentication Key might be required.

Check "**Source** is secured" if you want to use one.

___

###Source Information and Available Resources



![Remote Configuration, Source Information, and Available Resources][2]

Information about [Remote Configuration].


###Source Information

####Server, Environment and Version

Unchangeable Information about the Server, Environment and Version.


####Alive Check

Test the connection to the Source by clicking Check.

You can see the status for the last alive check as well as the time of the last check. 

![Source Information][3]

###Available Resources

Check all the available [Resources] within the Source.

The available information includes:

|	|
|---|
|Name	|
|Description	|
|Application	|
|Application	|
|Category	|

![Available Resources][4]

<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A64.%20Add%20Source.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/10531a44ed508d1a1ebca8e0871342bcba6b9b01/Media/Documentation%20Pictures/C14.%20Remote.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/10531a44ed508d1a1ebca8e0871342bcba6b9b01/Media/Documentation%20Pictures/C14.3%20Source%20Information.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/10531a44ed508d1a1ebca8e0871342bcba6b9b01/Media/Documentation%20Pictures/C14.2%20Available%20Resources.png

[Resources]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Remote Configuration]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master
[Source]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
___

###Next step
####Related
