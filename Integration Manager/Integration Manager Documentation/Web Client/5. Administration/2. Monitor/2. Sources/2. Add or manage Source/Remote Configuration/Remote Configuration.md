<tags data-value="Remote,Configuration,BizTalk,Source,Resources"></tags>

Remote Configuration
=====
					
[Done this? Next step](#next step)

___

##Information

The first step is to [add or manage a Source].

On the bottom of the page, you will find Remote Configuration. 

![Remote Configuration, Source Information, and Available Resources][1]

###Remote Configuration

There's three editions of the "Remote Configuration window"

These could be called standard, SQL, and BizTalk.

___

##Instructions

###Standard

####Environment and Reload

Name the type of target environment, for example: Test, Prod, or QA.

The Reload button is useful when you want to restore the file to the last time it was saved.

####Settings

Settings is the settings file which for example could be of XML or JSON.

You are able to edit the file directly in the window.



![Remote Configuration][2]

___

###SQL

####Environment and Reload

Name the type of target environment, for example: Test, Prod, or QA.

The Reload button is useful when you want to restore the file to the last time it was saved.

####Settings

Settings is the settings file which for example could be of XML or JSON.

You are able to edit the file directly in the window.

####Named connection string

The named connection strings for the databases to monitor by the SQL Monitor Agent.

It will also be restored to last save by the the Restore button.

![Remote Configuration 2][3]

___

###BizTalk

####Environment and Reload

Name the type of target environment, for example: Test, Prod, or QA.

The Reload button is useful when you want to restore the file to the last time it was saved.

####Connection String

The Connection String to the BizTalk management database.

####Max number of suspended instances to return

The maximum number of suspended messages to return. 

30 is the recommended setting.

Higher values may cause timeouts and excessive use of resources.



![Remote Configuration BizTalk][4]






<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/10531a44ed508d1a1ebca8e0871342bcba6b9b01/Media/Documentation%20Pictures/C14.%20Remote.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9d7addece82d1d7da52c04b222b8b66bf35c005c/Media/Documentation%20Pictures/C14.5%20Remote%20Windows%20Services.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9d7addece82d1d7da52c04b222b8b66bf35c005c/Media/Documentation%20Pictures/C14.6%20Remote%20SQL.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/10531a44ed508d1a1ebca8e0871342bcba6b9b01/Media/Documentation%20Pictures/C14.4%20Remote%20Configuration.png

[add or manage a Source]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
___

###Next step
####Related
