<tags data-value="View,All,Resources"></tags>

View All Resources
=====
					
[Done this? Next step](#next step)

___

##Information

The [Resources] overview shows all the **Resources** where you can filter by characters, edit, delete, and [Include Deleted].

This is the **Resources** overview.

![View All Resources][1]



<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A65.View%20All%20Resources.png

[Resources]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Include Deleted]: https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/8.%20Include%20Deleted/Include%20Deleted.md?at=master

___

###Next step
####Related
