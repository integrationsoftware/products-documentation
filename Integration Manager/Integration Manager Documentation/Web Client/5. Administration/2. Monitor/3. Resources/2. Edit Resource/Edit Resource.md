<tags data-value="Edit,Resources,Web,Site,Information"></tags>

Edit Resource
=====
					
[Done this? Next step](#next step)

___

##Information

A Web Site for additional information can be added while the other information is only displayed.

![Edit Resource][1]

<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/63e37e3d88f5e67ecc5bc572c18133b649c246a5/Media/Documentation%20Pictures/A88.%20Edit%20Resource.png
___

###Next step
####Related
