<tags data-value="Settings,Time,Intervals,Configurations,External"></tags>

Settings
=====
					
[Done this? Next step](#next step)

___

##Information

Settings is where you access [Time Intervals], [Time Interval Configurations], and [External Instances].  

<!--References -->

[External Instances]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/3.%20External%20Instances/External%20Instance.md?at=master
[Time Interval Configurations]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Time%20Interval%20Configurations/Time%20Interval%20Configurations.md?at=master
[Time Intervals]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Time%20Intervals/Time%20Intervals.md?at=master
___

###Next step
####Related
