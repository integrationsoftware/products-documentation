<tags data-value="Time,Intervals,Quantity,Unit"></tags>

Time Intervals
=====
					
[Done this? Next step](#next step)

___

##Information

A Time Interval is an interval of time made out of A Quantity and A Unit.

The Quantity is a positive Integer.

Time Intervals are used in [Time Interval Configurations] which in turn is used in [Log Views]

The Units to choose between is:

|	|
|---|
|Seconds	|
|Minutes	|
|Hours	|
|Days	|
|Weeks	|
|Months	|
|Years	|

For Example, the Quantity could be 42 and the Unit could be Minutes.

[How to add or manage a Time Interval].

<!--References -->
[Log Views]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Time Interval Configurations]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Time%20Interval%20Configurations/Time%20Interval%20Configurations.md?at=master
[How to add or manage a Time Interval]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Time%20Intervals/2.%20Add%20or%20manage%20Time%20Interval/Add%20or%20manage%20Time%20Interval.md?at=master

___

###Next step
####Related
