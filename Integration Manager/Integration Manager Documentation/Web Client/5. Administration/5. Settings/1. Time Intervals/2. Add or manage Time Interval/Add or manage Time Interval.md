<tags data-value="Add,Time,Interval,Quantity,Unit"></tags>

Add or manage Time Interval
=====
					
[Done this? Next step](#next step)

___

##Information

A [Time Interval] is made of a Quantity, such as 42, and a Unit, such as Minutes. 

##Instructions

![Add Time Intervals][1]

### Quantity and Unit

Quantity and Unit are required to create a Time Interval.

Enter a Quantity which should be a number that's a positive integer.

Choose a Unit of time, you can choose between:

|	|
|---|
|Seconds	|
|Minutes	|
|Hours	|
|Days	|
|Weeks	|
|Months	|
|Years	|

![Units][2]

#### Description

Adding a Description is optional.


<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A78.%20Add%20Time%20Interval.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A78.%20Time%20Interval%20Units.png

[Time Interval]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Time%20Intervals/Time%20Intervals.md?at=master

___

###Next step
####Related
