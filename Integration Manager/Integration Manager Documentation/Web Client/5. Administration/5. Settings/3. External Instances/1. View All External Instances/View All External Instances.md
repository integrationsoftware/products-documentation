<tags data-value="View,All,External,Instances"></tags>

View All External Instances
=====
					
[Done this? Next step](#next step)

___

##Information

The [External Instances] overview shows all the External Instances where you can filter by characters, edit, delete, [Include Deleted], and [Add or manage External Instance].

This is the External Instances overview.

![View All External Instances][1]



<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A82.%20View%20All%20External%20Instances.png

[Include Deleted]: https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/8.%20Include%20Deleted/Include%20Deleted.md?at=master
[External Instances]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/3.%20External%20Instances/External%20Instance.md?at=master
[Add or manage External Instance]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/3.%20External%20Instances/2.%20Add%20or%20manage%20External%20Instance/Add%20or%20manage%20External%20Instance.md?at=master
___

###Next step
####Related
