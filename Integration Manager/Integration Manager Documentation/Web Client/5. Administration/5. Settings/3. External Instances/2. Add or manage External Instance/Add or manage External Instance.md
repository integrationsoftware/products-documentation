<tags data-value="Add,External,Instance,URI"></tags>

Add or manage External Instance
=====
					
[Done this? Next step](#next step)

___

##Information

An [External Instance] is another instance of Integration Manager.

##Instructions

![Add External Instance][1]


A Name is required to create the External Instance.

Adding a Description is optional.

### URI

URI stands for Uniform Resource Identifier and is the link to the Web API.

It's written without api, for example: http://servername/IM/WebAPI

<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A83.%20Add%20External%20Instance.png

[External Instance]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/3.%20External%20Instances/External%20Instance.md?at=master
___

###Next step
####Related
