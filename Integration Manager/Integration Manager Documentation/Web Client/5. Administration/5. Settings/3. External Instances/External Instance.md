<tags data-value="External,Instance,Demo"></tags>

External Instance
=====
					
[Done this? Next step](#next step)

___

##Information

An External Instance is another instance of Integration Manager.

Using another instance of Integration Manager on the platform could be useful for demo purposes.

[How to add or manage an External Instance].

<!--References -->
[How to add or manage an External Instance]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/3.%20External%20Instances/2.%20Add%20or%20manage%20External%20Instance/Add%20or%20manage%20External%20Instance.md?at=master
___

###Next step
####Related
