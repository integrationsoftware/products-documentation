<tags data-value="Time,Interval,Configurations"></tags>

Time Interval Configurations
=====
					
[Done this? Next step](#next step)

___

##Information

A Time Interval Configuration consists of [Time Intervals] and allows Detailed Time Search if chosen.

Time Interval Configurations are used in [Log Views].

[How to add or manage a Time Interval Configuration].
<!--References -->

[Time Intervals]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Time%20Intervals/Time%20Intervals.md?at=master
[Log Views]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/Log.md?at=master
[How to add or manage Time Interval Configurations]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Time%20Interval%20Configurations/2.%20Add%20Time%20Interval%20Configurations/Add%20Time%20Interval%20Configurations.md?at=master
___

###Next step
####Related
