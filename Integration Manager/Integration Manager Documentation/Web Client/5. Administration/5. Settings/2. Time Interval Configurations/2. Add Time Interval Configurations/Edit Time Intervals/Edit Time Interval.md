<tags data-value="Edit,Time,Interval"></tags>

Edit Time Interval
=====
					
[Done this? Next step](#next step)

___

##Information

Choose [Time Intervals] to add to a [Time Interval Configurations]. 

##Instructions

You can Add, Filter and Remove Time Intervals.

![Add Time Interval Configuration][1]



<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A81.%20Edit%20Time%20Intervals.png

[Time Intervals]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Time%20Intervals/Time%20Intervals.md?at=master
[Time Interval Configurations]:https://bitbucket.org/integrationsoftware/products-documentation/src/2d001bd0884a6d3bbad27d5ff41aaa607a8f9e6c/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Time%20Interval%20Configurations/Time%20Interval%20Configurations.md?at=master
___

###Next step
####Related
