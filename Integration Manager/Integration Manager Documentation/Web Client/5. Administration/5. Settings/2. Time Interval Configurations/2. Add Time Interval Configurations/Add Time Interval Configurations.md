<tags data-value="Add,Time,Interval,Configurations"></tags>

Add or manage Time Interval Configurations
=====
					
[Done this? Next step](#next step)

___

##Information

A Time Interval Configuration consists of [Time Intervals] and allows Detailed Time Search if chosen.

##Instructions

![Add Time Interval Configuration][1]

### Name and Description

A Name is required to create the Time Interval Configuration.

Adding a Description is optional. 

### Allow Detailed Time Search

Check box to allow Detailed Time Search. 

Otherwise, the only time options available is the ones included in the Time interval Configuration


<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A80.%20Add%20Time%20Interval%20Configuration.png

[Time Intervals]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Time%20Intervals/Time%20Intervals.md?at=master
___

###Next step
####Related
