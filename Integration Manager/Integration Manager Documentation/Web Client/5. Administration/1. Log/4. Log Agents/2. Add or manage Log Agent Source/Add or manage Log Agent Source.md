<tags data-value="Add,Log,Agent,Source,Value"></tags>

Add Log Agent Source
=====
					
[Done this? Next step](#next step)

___

##Information

A [Log Agent] is a custom built agent which gets information from a source within a certain interval.



##Instructions

Name, Log Agent Value Id and URL are required while a Description is optional.

![Add Log Agent][1]



####Name

Name the **Log Agent Source**.

####Log Agent Value Id

Log Agent Value Id is the value of the **Log Agent Source**.

####Description

Add additional information about the **Log Agent Source**.

####URL

The URL to the Log Agent.

<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/e1cb124b20856e338518eff58d42bcb3da363b59/Media/Documentation%20Pictures/A54.%20Add%20Log%20Agent.png

[Log Agent]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
___

###Next step
####Related
