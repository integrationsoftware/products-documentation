<tags data-value="Log,Agents,Interval"></tags>

Log Agents
=====
					
[Done this? Next step](#next step)

___

##Information

A Log Agent is a custom built agent which gets information from a source within a certain interval.

An agent can either fetch information when it's asked to or all the time.

If the update interval is short, the agent should be fetching all the time to have it ready when asked. 

[How to add or manage Log Agents].

<!--References -->
[How to add or manage Log Agents]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/2.%20Add%20or%20manage%20Log%20Agent%20Source/Add%20or%20manage%20Log%20Agent%20Source.md?at=master
___

###Next step
####Related
