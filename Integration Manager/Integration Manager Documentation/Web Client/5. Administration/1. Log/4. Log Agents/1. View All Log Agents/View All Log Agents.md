<tags data-value="View,All,Log,Agents,Sources"></tags>

View All Log Agents
=====
					
[Done this? Next step](#next step)

___

##Information

The [Log Agents] overview shows all the **Log Agents** where you can filter by characters, edit, delete, [Include Deleted], and [Add or manage Log Agents].

This is the **Log Agent Sources** overview.

![View All Log Status Codes][1]



<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/e1cb124b20856e338518eff58d42bcb3da363b59/Media/Documentation%20Pictures/A53.%20View%20All%20Log%20Agents.png

[Include Deleted]: https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/8.%20Include%20Deleted/Include%20Deleted.md?at=master
[Log Agents]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
[Add or manage Log Agents]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/2.%20Add%20or%20manage%20Log%20Agent/Add%20or%20manage%20Log%20Agent.md?at=master
___

###Next step
####Related
