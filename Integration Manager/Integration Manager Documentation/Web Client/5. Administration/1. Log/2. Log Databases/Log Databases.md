<tags data-value="Log,Monitor"></tags>

Log Databases
=====
					
[Done this? Next step](#next step)

___

##Information

**Log Databases** are servers used to log data.

You can choose start and end dates when the database should log data.

|The available information about the Log Databases:	|
|---|
|Database	|
|Server	|
|Description	|
|Start Date	|
|End Date	|
|Read Only	|

[How to add or manage a Log Database].

<!--References -->	
[How to add or manage a Log Database]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Log%20Databases/2.%20Add%20or%20manage%20Log%20Database/Add%20or%20manage%20Log%20Database.md?at=master
___

###Next step
####Related
