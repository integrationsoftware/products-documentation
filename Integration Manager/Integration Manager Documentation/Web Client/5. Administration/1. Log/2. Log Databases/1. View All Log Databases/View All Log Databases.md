<tags data-value="Log,Database,View,All"></tags>

View All Log Databases
=====
					
[Done this? Next step](#next step)

___

##Information

The [Log Database] overview shows all the **Log Databases** where you can filter by characters, edit, and [Add or manage Log Database].

|The available information in the view:	|
|---|
|Database	|
|Server	|
|Description	|
|Start Date	|
|End Date	|
|Read Only	|	



![Log Databases][1]

You cannot delete a database through this section, though you can edit through Action.

![Action][2]


<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/a3d7c4bfa73400eb8b348190f0431cb0a3dc22e1/Media/Documentation%20Pictures/A49.%20Log%20databases.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/a7238e758b9f032da9e0eb6ee4c9e2789aaa27cb/Media/Documentation%20Pictures/A49.2%20Action.png

[Log Database]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Log%20Databases/Log%20Databases.md?at=master
[Add or manage Log Database]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Log%20Databases/2.%20Add%20or%20manage%20Log%20Database/Add%20or%20manage%20Log%20Database.md?at=master


___

###Next step
####Related
