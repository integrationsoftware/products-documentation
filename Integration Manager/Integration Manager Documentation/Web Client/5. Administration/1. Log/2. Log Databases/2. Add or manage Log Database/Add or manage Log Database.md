<tags data-value="Add,Log,Database,Server"></tags>

Add or manage Log Database
=====
					
[Done this? Next step](#next step)

___

##Information

A [Log Database] is a server used to log data.


##Instructions

![Add Log Database][1]

####Name

A Database name is required to create the **Log Database**.

#### Server

Add a name of the Server. It should be 'localhost' if the log database is located on the same server as the configuration database.

#### Read Only

Check the box if this log database should be read only. Integration Manager will use this database to only search for data and not to reindex, remove, or add.

Read Only can only be changed if end time has passed current date.

#### Remote Server

Check the box if the database is located on a remote server. 

#### Start date and End date

The date and time used or will use to log data as well as the date and time stopped or stops using to log data.


![Start Date][2]

####Description

Adding a Description is optional.







<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/7561c313e5f193405fdc912a55fce939bae29baa/Media/Documentation%20Pictures/A50.%20Add%20Log%20Database.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/bf49dc99989a8cc4f1a97fc528f9e5e205331b0a/Media/Documentation%20Pictures/A50.2%20Start%20Date.png
[Log Database]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Log%20Databases/Log%20Databases.md?at=master
___

###Next step
####Related
