<tags data-value="Log,Add,View"></tags>

Add or manage Log View
=====
					
[Done this? Next step](#next step)

___


[Add or manage Log View]


<!--References -->

[Add or manage Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/Create%20New%20Log%20View.md?at=master
___

###Next step
####Related
