<tags data-value="Log,Views,Databases,Status,Search"></tags>

Log
=====
					
[Done this? Next step](#next step)

___

##Information

The Log will give you access to [Log Views], [Log Databases], [Log Status Codes], [Log Agents], and [Search Fields] to control the **Log Views**

<!--References -->
[Log Views]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Log Databases]:https://bitbucket.org/integrationsoftware/products-documentation/src/3master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Log%20Databases/Log%20Databases.md?at=master
[Log Status Codes]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/3.%20Log%20Status%20Codes/Log%20Status%20Codes.md?at=master
[Log Agents]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
[Search Fields]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/Search%20Fields.md?at=master

___

###Next step
####Related
