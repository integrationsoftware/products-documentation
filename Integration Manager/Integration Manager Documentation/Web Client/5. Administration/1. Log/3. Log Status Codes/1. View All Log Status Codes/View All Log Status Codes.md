<tags data-value="View,All,Log,Status,Codes"></tags>

View All Log Status Codes
=====
					
[Done this? Next step](#next step)

___

##Information


The [Log Status Codes] overview shows all the **Log Status Codes** where you can filter by characters, edit, delete, [Include Deleted], and [Add or manage Log Status Codes].

This is the **Log Status Codes** overview.


![Log Status Codes][1]

|The available information in the view:	|
|---|
|Log Status	|
|Log Agent Source	|
|Description	|



<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/22edd44cc722cfd57edee9dafd171995eee9306d/Media/Documentation%20Pictures/A51.%20Log%20Status%20Codes.png

[Log Status Codes]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/3.%20Log%20Status%20Codes/Log%20Status%20Codes.md?at=master
[Add or manage Log Status Codes]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/3.%20Log%20Status%20Codes/2.%20Add%20or%20manage%20Status%20Code/Add%20or%20manage%20Status%20Code.md?at=master
[Include Deleted]: https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/8.%20Include%20Deleted/Include%20Deleted.md?at=master

___

###Next step
####Related
