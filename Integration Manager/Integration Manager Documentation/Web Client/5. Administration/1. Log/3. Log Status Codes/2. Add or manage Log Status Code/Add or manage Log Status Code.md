<tags data-value="Add,Log,Status,Code"></tags>

Add Log Status Code
=====
					
[Done this? Next step](#next step)

___

##Information

A [Log Status Code] is a number which shows the status of an event or a message.

Log Status, Log Agent Source, and Description are required while the URL is optional. 

##Instructions

![Add Log Status Code][1]

#### Log status

The Log Status Code is the number which shows the status of a logged event.

For example, 0 in BizTalk means ok.

#### Log Agent Source

Log Agent Source is the system or application from which the events are being logged.

#### Description and URL

Add a Description and a URL for additional information. 


<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/de10a557960d9348a0eec7062e737eb2e8b7a319/Media/Documentation%20Pictures/A52.%20Add%20Log%20Status%20Code.png

[Log Status Code]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/3.%20Log%20Status%20Codes/Log%20Status%20Codes.md?at=master
___

###Next step
####Related
