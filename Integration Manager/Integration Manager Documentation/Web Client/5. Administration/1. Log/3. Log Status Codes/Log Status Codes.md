<tags data-value="Log,Status,Codes"></tags>

Log Status Codes
=====
					
[Done this? Next step](#next step)

___

##Information

A Log Status Code is a number which shows the status of an event or a message.

For example, 0 in BizTalk means ok.

[How to add or manage a Log Status Code].

<!--References -->

[How to add or manage a Log Status Code]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/3.%20Log%20Status%20Codes/2.%20Add%20or%20manage%20Log%20Status%20Code/Add%20or%20manage%20Log%20Status%20Code.md?at=master
___

###Next step
####Related
