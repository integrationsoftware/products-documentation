<tags data-value="Key,Value,Extract,Expression,Search"></tags>

Key
=====
					
[Done this? Next step](#next step)

___

##Information

Key is an Expression Type which extracts a value from a key value collection.



Expression Types are used in [Search Fields].



The Expression Types to choose between when configuring a Search Field is:

|	|
|---|
|[Flat File Fixed Width]	|
|Key	|
|[RegEx]	|
|[XPath]	|


How to [Configure Search Field Expression].

How to [Add or manage Search Field].

___

##Instructions

###Key

This default extractor finds the provided key in the message context and grabs the value.



![Key][1]



####Test Expression

Key cannot be tested in the Test Expression tab because it needs a logged message to extract values from.



<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/e0daff021cff3cd95fc5337679a9b378097cda1c/Media/Documentation%20Pictures/B7.%20Key.png

[Search Fields]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/Search%20Fields.md?at=master
[Add or manage Search Field]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/2.%20Add%20Search%20Field/Add%20or%20manage%20Search%20Field.md?at=master
[Configure Search Field Expression]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/2.%20Add%20Search%20Field/Configure%20Search%20Field%20Expression/Configure%20Search%20Field%20Expression.md?at=master
[RegEx]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Configure%20Search%20Field%20Expression/3.%20RegEx/RegEx.md?at=master
[XPath]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Configure%20Search%20Field%20Expression/4.%20XPath/XPath.md?at=master
[Flat File Fixed Width]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Configure%20Search%20Field%20Expression/1.%20Flat%20File%20Fixed%20Width/Flat%20File%20Fixed%20Width.md?at=master
___

###Next step
####Related
