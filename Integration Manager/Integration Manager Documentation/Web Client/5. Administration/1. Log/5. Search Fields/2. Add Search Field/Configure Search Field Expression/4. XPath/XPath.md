<tags data-value="XPath,XML,Expression,Message"></tags>

XPath
=====
					
[Done this? Next step](#next step)

___

##Information

XPath is an Expression Type which extracts a value from a message.

Expression Types are used in [Search Fields].



The Expression Types to choose between when configuring a Search Field is:

|	|
|---|
|[Flat File Fixed Width]	|
|[Key]	|
|[RegEx]	|
|XPath	|


How to [Configure Search Field Expression].

How to [Add or manage Search Field].

___

##Instructions


###XPath

This default extractor uses a high performance read-only 
fast forward only stream reader. 

Not all types of XPaths can be evaluated.

___

An example of an expression is:

/*/

	Example: /*/from will get the value within <from> </from>
	
You can find more expressions [here].


![XPath][1]



####Test Expression

You can test an expression when configuring a Search Field in the Test Expression tab.


![Test Expression][2]
 
<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/5798b1d000859b39377d8cb7aab830876a8b9d70/Media/Documentation%20Pictures/B9.%20Xpath.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/4c379aafc4f404b078c2189e1c597cd6ab2f5291/Media/Documentation%20Pictures/B9.2%20Test%20XPath.png
[Search Fields]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/Search%20Fields.md?at=master
[Add or manage Search Field]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/2.%20Add%20Search%20Field/Add%20or%20manage%20Search%20Field.md?at=master
[Configure Search Field Expression]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/2.%20Add%20Search%20Field/Configure%20Search%20Field%20Expression/Configure%20Search%20Field%20Expression.md?at=master
[Key]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Configure%20Search%20Field%20Expression/2.%20Key/Key.md?at=master
[RegEx]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Configure%20Search%20Field%20Expression/3.%20RegEx/RegEx.md?at=master
[Flat File Fixed Width]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Configure%20Search%20Field%20Expression/1.%20Flat%20File%20Fixed%20Width/Flat%20File%20Fixed%20Width.md?at=master
[here]:https://msdn.microsoft.com/en-us/library/ms256086(v=vs.110).aspx
___

###Next step
####Related
