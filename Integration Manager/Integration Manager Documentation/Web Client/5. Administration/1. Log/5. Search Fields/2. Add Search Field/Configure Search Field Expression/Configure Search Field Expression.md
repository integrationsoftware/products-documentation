<tags data-value="Configure,Search,Field,Expression,Test"></tags>

Configure Search Field Expression
=====
					
[Done this? Next step](#next step)

___

##Information

A Search Field Expression is a limitation of a Search Field.

Expression Type and Message Type is required but Expression is optional. 

##Instructions

####Expression

The Expression is used to select specific values to extract.

####Expression Type

Expression Type is the type of expression or plugin to extract values from the message.

####Message Type

Which [Message Type] to apply this extraction of values on.

###Search Field Expression

![Search Field Expression][5]

###Expression Types

The Expression Types to choose between is:

|	|
|---|
|Flat File Fixed Width	|
|Key	|
|RegEx	|
|XPath	|

![Expression Type 1][1]

![Expression Type 2][2]

####Message Type


![Message Type][3]


###Processing State

There are three types of processing states which is expressed as a cogwheel with a green, yellow or red sign.


![Functional Test][8]


![Processed with Warning][4]


![Failed Test][9]

####Update Expression

When edit is clicked, you will be able to update the Search Field Expression.

![Update][7]

####Link to Message Type

By clicking the name of the Message Type, you will be able to edit it.

![Link To Message Type][6]




<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/bbf7dc41fcf8fc9c206128109156f32ad67d43f1/Media/Documentation%20Pictures/A57.2%20Expression%20Type.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/bbf7dc41fcf8fc9c206128109156f32ad67d43f1/Media/Documentation%20Pictures/A57.2%20Expression%20Type%202.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/bbf7dc41fcf8fc9c206128109156f32ad67d43f1/Media/Documentation%20Pictures/A57.3%20Message%20Type.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/bbf7dc41fcf8fc9c206128109156f32ad67d43f1/Media/Documentation%20Pictures/A57.4%20Test%20Expression.png
[5]:https://bytebucket.org/integrationsoftware/products-documentation/raw/bbf7dc41fcf8fc9c206128109156f32ad67d43f1/Media/Documentation%20Pictures/A57.%20Search%20Field%20Expression.png
[6]:https://bytebucket.org/integrationsoftware/products-documentation/raw/bbf7dc41fcf8fc9c206128109156f32ad67d43f1/Media/Documentation%20Pictures/A57.8%20Link%20to%20Message%20Type.png
[7]:https://bytebucket.org/integrationsoftware/products-documentation/raw/bbf7dc41fcf8fc9c206128109156f32ad67d43f1/Media/Documentation%20Pictures/A57.5%20Update.png
[8]:https://bytebucket.org/integrationsoftware/products-documentation/raw/bbf7dc41fcf8fc9c206128109156f32ad67d43f1/Media/Documentation%20Pictures/A57.6%20Test%20Expression%20Fine.png
[9]:https://bytebucket.org/integrationsoftware/products-documentation/raw/bbf7dc41fcf8fc9c206128109156f32ad67d43f1/Media/Documentation%20Pictures/A57.7%20Test%20Expression%20Failed.png

[Message Type]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
___

###Next step
####Related
