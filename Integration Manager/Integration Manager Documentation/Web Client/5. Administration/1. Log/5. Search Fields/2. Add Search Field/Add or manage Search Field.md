<tags data-value="Add,Search,Field,Data,Type"></tags>

Add or manage Search Field
=====
					
[Done this? Next step](#next step)

___

##Information

A [Search Field] is a field where you are able to search for certain log events.

##Instructions

![View All Search Fields][1]

A Name is required to create the **Search Field**.

Adding a Description and a Website is optional. 

You will be able to search for the **Search Field** by the text in the Description.

####Data Types

The Data Types to choose between is:

|	|
|---|
|Text	|
|Integer	|
|Long Integer	|
|Real number with 2 decimals	|

![Data Type][3]

####Configured Search Field expressions

You are able to add an expression to the Search Field through Edit.

![Configured Search Field expressions][2]

<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/576ebcaa40451e8eda87352c525790420ad68cb8/Media/Documentation%20Pictures/A56.%20Add%20Search%20Field.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/404878748a28bfc0d1eb10a7c0b3bd37e82b6497/Media/Documentation%20Pictures/A56.2%20Configured%20Search%20Field%20expression.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/54fe944440ea6f345bcf68a0342eaa53beffc5e3/Media/Documentation%20Pictures/A56.3%20Data%20Type.png

[Search Field]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/Search%20Fields.md?at=master
___

###Next step
####Related
