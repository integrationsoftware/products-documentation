<tags data-value="Search,Fields,Expression,Restrictions"></tags>

Search Fields
=====
					
[Done this? Next step](#next step)

___

##Information

A Search Field is a field where you are able to search for certain log events through restrictions.

You can add an Expression to the Search Field to make the restriction more advanced and precise.

[How to add a Search Field].


<!--References -->

[How to add a Search Field]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/2.%20Add%20Search%20Field/Add%20or%20manage%20Search%20Field.md?at=master

___
###Next step
####Related
