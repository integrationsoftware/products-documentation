<tags data-value="Notifications,Alarm,Plugins,Email"></tags>

Notifications
=====
					
[Done this? Next step](#next step)

___

##Information

Notifications is where you access the [Alarm Plugins].

<!--References -->

[Alarm Plugins]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/6.%20Notifications/Alarm%20Plugins/Alarm%20Plugins.md?at=master
___

###Next step
####Related
