<tags data-value="Edit,Email,Alarm,Plugin,Mail"></tags>

Edit the email plugin
=====
					
[Done this? Next step](#next step)

___

##Information

The email plugin is an [Alarm Plugin] that sends an email when the status of a [Log View] changes.

Emails are sent both when errors occur and when it's solved. 

##Instructions




####Description and Web Site

Edit or add a Description and a Web Site to add information about the usage of the plugin.

####Stylesheet

A [Stylesheet] may be selected to format the email. 

![Alarm Plugin][1]



####Web Client URL

Enter the URL to Integration Manager.

####Subject

Enter the subject of the email.

You can also add a variable. Choose between {Customer}, {Environment}, and {Version}.

####Test Mail To

Enter an email adress which will be sent test mails.

####From

Enter an email adress which the test mail will be sent from.

![Alarm Plugin 2][2]



####Server

Enter an SMTP server, a DNS name or an IP-adress.

####Port

Enter the port used to connect to an SMTP server.

The standards for SSL are 25 or 465.

####Enable SSL 

Check the box if an SSL connection should be used to connect to an SMTP server.

####Use Authentication

Check the box if Authentication with an SMTP server should be used and enter User Name and Password.

![Alarm Plugin 3][3]


####Execute and Restore Alarm test

When an Alarm test is executed, an email with error status will be sent to the Test Mail address.

If no mail appears, check the junk mail.

Restore Alarm test is similar to Execute Alarm but instead sends an OK status mail.

___

Here's an example of an executed test email with a Stylesheet.

![Email][4]

<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/ef89d626d0c916386a52005d6ca22023b6eb6135/Media/Documentation%20Pictures/C11.%20Alarm%20Plugin.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/ef89d626d0c916386a52005d6ca22023b6eb6135/Media/Documentation%20Pictures/C11.2%20Alarm%20Plugin%202.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/ef89d626d0c916386a52005d6ca22023b6eb6135/Media/Documentation%20Pictures/C11.3%20Alarm%20Plugin%203.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/1db44f48846227a24b9e929adf82b1420be4a269/Media/Documentation%20Pictures/C12.%20Email.png

[Stylesheet]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/4.%20Templating/Stylesheets/Stylesheets.md?at=master
[Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Alarm Plugin]:https://bitbucket.org/integrationsoftware/products-documentation/src/e6e1c059654cbc021626de3e9bd359894da75d36/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/6.%20Notifications/Alarm%20Plugins/View%20All%20Alarm%20Plugins/View%20All%20Alarm%20Plugins.md?at=master
___

###Next step
####Related
