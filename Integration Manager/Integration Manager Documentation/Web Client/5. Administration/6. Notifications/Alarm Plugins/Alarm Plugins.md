<tags data-value="Alarm,Plugins,Email,Stylesheet"></tags>

Alarm Plugins
=====
					
[Done this? Next step](#next step)

___

##Information

An Alarm Plugin is for example an email setup that sends an email every time the status changes on a Log.

A [Stylesheet] can be added to an Alarm to format the message.

<!--References -->

[Stylesheet]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/4.%20Templating/Stylesheets/Stylesheets.md?at=master
___

###Next step
####Related
