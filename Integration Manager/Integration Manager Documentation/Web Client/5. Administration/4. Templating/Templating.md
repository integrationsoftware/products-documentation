<tags data-value="Templating,Stylesheets,Format"></tags>

Templating
=====
					
[Done this? Next step](#next step)

___

##Information

Templating is the link to [Stylesheets] which will let you format distributions like e-mail.

<!--References -->

[Stylesheets]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/4.%20Templating/Stylesheets/Stylesheets.md?at=master
___

###Next step
####Related
