<tags data-value="Stylesheets,XSLT,Format,Email"></tags>

Stylesheets
=====
					
[Done this? Next step](#next step)

___

##Information

A Stylesheet is formatting, for example, an e-mail to make it look better and easier to understand.

Stylesheets are made with XSLT in Integration Manager.

[How to add or manage a Stylesheet].

<!--References -->
[How to add or manage a Stylesheet]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/4.%20Templating/Stylesheets/2.%20Add%20Stylesheet/Add%20or%20manage%20Stylesheet.md?at=master

___

###Next step
####Related
