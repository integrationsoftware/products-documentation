<tags data-value="Add,Stylesheet,Preview,XSLT,File"></tags>

Add Stylesheet
=====
					
[Done this? Next step](#next step)

___

##Information

A [Stylesheet] can be added to messages and notifications to format files and distributions like e-mail.

##Instructions

####Name and Description

A Name is required to create the Stylesheet.

Adding a Description is optional. 

####XSLT

XSLT stands for EXtensible Stylesheet Language Tranformations, and is a style sheet language for XML documents.

Use XSLT to format the Stylesheet.

![Add Stylesheet][1]

[Message Types] can be added to Stylesheets.

![Message Type][2]

###Preview Setup

Clicking Preview Setup will let you select data input, choose between File, Raw, or Event Id.

####Preview File

Select a File through browsing or drag and drop it within the field.

![Preview File][3]

####Preview Raw

Insert message data to preview.

![Preview Raw][4]

####Preview Event 

Select a Log Database, an Event Id and Message Data.

![Preview Event Id][5]




<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A75.%20Add%20Stylesheet.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A75.%20Message%20Types.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A75.2%20Stylesheet%20preview%20File.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A75.3%20Stylesheet%20preview%20Raw.png
[5]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A75.4%20stylesheet%20preview%20Event.png

[Message Types]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
[Stylesheet]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/4.%20Templating/Stylesheets/Stylesheets.md?at=master
___

###Next step
####Related
