<tags data-value="Stylesheets,Message Type"></tags>

Connect Message Type to Stylesheet
=====
					
[Done this? Next step](#next step)

___

##Information

Connect a [Message Type] to a [Stylesheet] to apply the Template to certain Message Types.

More information about usage: [View Message as formatted]. 

##Instructions

You can Add, Filter and Remove Message Types.

![View All Stylesheets][1]

<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A76.%20Connect%20Message%20Types.png

[Message Type]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
[View Message as formatted]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/4.%20How%20to%20Search%20the%20Log%20View/View%20Message%20as%20formatted/View%20Message%20as%20formatted.md?at=master
[Stylesheet]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/4.%20Templating/Stylesheets/Stylesheets.md?at=master
___

###Next step
####Related
