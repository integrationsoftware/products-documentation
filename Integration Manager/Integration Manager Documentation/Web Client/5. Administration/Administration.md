<tags data-value="Administration,Log,Monitor,Authorization,Settings"></tags>

Administration
=====
					
[Done this? Next step](#next step)

___

##Information

Administration is where you access the [Log], [Monitor], [Authorization], [Templating], [Settings], and [Notifications]

This is the Administration overview.

![Administration][1]

<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/0f6ff215e1ff084e3fc9d7218518df4261118b56/Media/Documentation%20Pictures/A93.%20Administration.png

[Log]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/Log.md?at=master
[Monitor]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/Monitor.md?at=master
[Authorization]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master
[Templating]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/4.%20Templating/Templating.md?at=master
[Settings]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/Settings.md?at=master
[Notifications]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/6.%20Notifications/Notifications.md?at=master


___

###Next step
####Related
