<tags data-value="Mathematical,Search,Operators,Restrictions"></tags>

Mathematical Search Operators
=====
					
[Done this? Next step](#next step)

___

##Information

Mathematical Operators are used in [Restrictions] and in the Additional Fields within a [Log].


| Symbol  |Meaning   |
|---|---|
|=   |Equals   |
|<>   |Isn't Equal to   |
|Like   |Like any bofore or after provided value   |
|Left Like   |Like any before provided value   |
|Right Like   |Like any after provided value   |
|Not Like   |Like no provided value, before or after   |
|Left Not like   |Like not any before provided value   |
|Right Not Like   |Like not any after provided value   |
|>=   |Equal or bigger than   |
|>  |Bigger than   |
|<   |Less than   |
|<=   |Equal or less than   |


Although, they look a bit different:

Additional Fields

![Mathematical Search Operators][1]
![Alternative][2]

Restrictions

![Restrictions][3]
![More Alternatives][4]

<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/a69e2d7f2cd7c3aa05a3085acc993113a1e3da0c/Media/Documentation%20Pictures/A15.%20Mathematical%20Search%20Parameters.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/3201d02d4f3ab5803c6e83d95bf583d0424bb82c/Media/Documentation%20Pictures/A15.%20Alternative.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/f41b2d52c2d0371e673e949b8ae74e1490a85a43/Media/Documentation%20Pictures/A15.3%20Search%20restriction1.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/f41b2d52c2d0371e673e949b8ae74e1490a85a43/Media/Documentation%20Pictures/A15.3%20Search%20restriction2.png

[Restrictions]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/2.%20Log/2.%20Create%20%20New%20Log%20View/2.%20Restrictions/Restrictions.md?at=master
[Log]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/2.%20Log/Log.md?at=master
___

###Next step
####Related
