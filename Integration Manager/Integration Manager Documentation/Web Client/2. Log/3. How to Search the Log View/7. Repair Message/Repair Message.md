<tags data-value="Repair,Message,File,MSMQ,Queue"></tags>

Repair Message
=====
					
[Done this? Next step](#next step)

___

##Information

Repair Message is useful when you want to edit a message, for example the destination or the message.



####Allow repair of messages



First, make sure "Allow resend of messages" and "Allow repair of messages" is checked in the edit view of the Log View.

![Allow Repair][7]

More information is available in [Add or manage Log View].

___

##Instructions

Click "View details" on an event while being in a Log View.

![New Tab][1]

###Repair and submit



In the upper right corner, you will find "Repair and Submit".

![Repair][2]

###Repair message

####File

Editing the message is optional.

File name is pre-filled and editable.

A Destination is required, could be C:\ for example.


![Repair Message][3]

###Encoding

There are several different encodings to choose from.

|Alternatives	|
|---|
|us-ascii	|
|Windows-1252	|
|iso-8859-1	|
|utf-16	|
|utf-8	|

![Encoding][5]

#### MSMQ

MSMQ stands for Microsoft Message Queue and is a way to queue messages. 

It provides guaranteed message delivery, routing, security, and priority-based messaging.

There's two differences between repairing a File and an MSMQ.

1. You can choose if the queue should be transactional.

2. You need to choose a message Label.

![Repair Message MSMQ][4]



###Error

You will get an error if the path is faulty. 

![Error][6]





<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/5fb9628ca74a1c6d5889c6e339e1c2b55d680892/Media/Documentation%20Pictures/A12.2%20View%20Details.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9ca5df5aa3f5aa7ad0604271d300ee159c631224/Media/Documentation%20Pictures/A99.%20Repair%20Message.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/de9c2c547ba81e06c2ab62269e976145b0b8c403/Media/Documentation%20Pictures/A99.3%20Edit%20Message.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/de9c2c547ba81e06c2ab62269e976145b0b8c403/Media/Documentation%20Pictures/A99.2%20Edit%20Message%20MSMQ.png
[5]:https://bytebucket.org/integrationsoftware/products-documentation/raw/de9c2c547ba81e06c2ab62269e976145b0b8c403/Media/Documentation%20Pictures/A99.4%20Encoding.png
[6]:https://bytebucket.org/integrationsoftware/products-documentation/raw/de9c2c547ba81e06c2ab62269e976145b0b8c403/Media/Documentation%20Pictures/A99.5%20Repair%20Error.png
[7]:https://bytebucket.org/integrationsoftware/products-documentation/raw/486a94b4552ec81d9456ad984324f4764cc1dfa2/Media/Documentation%20Pictures/A99.6%20Allow%20repair.png

[Add or manage Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
___

###Next step
####Related
