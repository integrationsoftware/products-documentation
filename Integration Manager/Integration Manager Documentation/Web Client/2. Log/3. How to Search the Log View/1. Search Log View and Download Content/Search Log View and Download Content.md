<tags data-value="Log,Search,View,Download"></tags>

Search Log and Download Content
=====
					
[Done this? Next step](#next step)

___

##Information



More information about [Logs].

##Instructions

###Show Log View

To show a **Log View**, click the name.


![Show Log][1]

####Show this View

While being at the edit page, you can use "Show this view".

Clone will create a copy of the Log and take you to the edit page.




![Show this view][5]

Export will open a dialogue window and ask which External Instance you want to export the Log View to.  


![Export Log View][8]

While being at the **Log View**, there wll also be an option to Change View.

![Change View][9]



####Choose Time Interval

Then choose a Time Range and hit Search.

A Custom Range may be selected.

![Time Range][6]

![Search][2]

####Additional Fields

You can add an Additional Field to get extra alternatives to search by.

####Search Fields

[How to use Search Fields in Log Views].

More information about [Search Fields].

###Download Log Content

You can Download the Raw Message under Action as a .txt file.

View Raw Message under Action will open a new tab in your browser.

![Actions][4]

####Select Messages

Select All Messages by checking the box to the left of Log Date.

You can also select multiple messages by selecting multiple boxes.

![Select All][7]

Save the selected messages as a .zip file which will contain .txt's as well.  



![With Selected][3]




<!--References -->

[Logs]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/2.%20Log/Log.md?at=master

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/743171bfee141e7745b30bdf2221f82b53f45d20/Media/Documentation%20Pictures/9.%20Show%20Log.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/579eb0d095c706e9bc8ac139cc542f90ec1a8790/Media/Documentation%20Pictures/A11.3%20Time%20Interval%20and%20Search.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/579eb0d095c706e9bc8ac139cc542f90ec1a8790/Media/Documentation%20Pictures/A11.2%20Select%20Log.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/00e15c8a3d307f1d390b43035852ac563bde0568/Media/Documentation%20Pictures/A12.3%20Actions.png
[5]:https://bytebucket.org/integrationsoftware/products-documentation/raw/f6025aa241a64f4b0c62d23a3d5b69512af8c828/Media/Documentation%20Pictures/9.2%20Show%20this%20view.png
[6]:https://bytebucket.org/integrationsoftware/products-documentation/raw/0937379a2df0bf7f2aa33940baec7a6b716ab9db/Media/Documentation%20Pictures/A95%20Log%20Time%20Range.png
[7]:https://bytebucket.org/integrationsoftware/products-documentation/raw/a9aab1412e683d35dc4c1239819948229a2797d7/Media/Documentation%20Pictures/A96.5%20Select%20All%20Messages.png
[8]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9835d7a3025ca475c13caea5dd1a190f752fcce2/Media/Documentation%20Pictures/B3.%20Export%20Log%20View.png
[9]:https://bytebucket.org/integrationsoftware/products-documentation/raw/d47c4c87063d345739a9682a24d7db34e154385b/Media/Documentation%20Pictures/B3.2%20Change%20View.png

[How to use Search Fields in Log Views]:https://bitbucket.org/integrationsoftware/products-documentation/src/ef08c79941c19f4f42cb242558a6dba6210f107d/Integration%20Manager/Integration%20Manager%20Documentation/2.%20Log/7.%20Search%20Fields%20In%20Log%20View/Search%20Fields%20In%20Log%20View.md?at=master
[Search Fields]:https://bitbucket.org/integrationsoftware/products-documentation/src/ef08c79941c19f4f42cb242558a6dba6210f107d/Integration%20Manager/Integration%20Manager%20Documentation/5.%20Administration/1.%20Log/5.%20Search%20Fields/Search%20Fields.md?at=master
___

###Next step
####Related
