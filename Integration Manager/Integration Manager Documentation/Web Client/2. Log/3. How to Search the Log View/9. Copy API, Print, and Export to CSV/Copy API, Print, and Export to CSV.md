<tags data-value="Copy,API,Print,Export,CSV"></tags>

Copy API, Print, and Export to CSV
=====
					
[Done this? Next step](#next step)

___

##Information

In the [Log View], there's options to Copy API URL, Print, and Export to CSV.

![Options][1]

##Instructions

####Copy API URL

Enable PowerBI support with Microsoft Excel. Simply copy the URL from the text box and use it with Power Query. 

Data is in Json format and is protected by your Windows login and can be consumed by many other clients tools.

![Copy API URL][2]

####Print Log View

Opens up a print friendly version of the **Log View** in a new tab.

![Print Log View][3]

####CSV File

This is an example of a CSV File in Excel.

![CSV File][4]




<!--References -->

[Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/2.%20Log/Log.md?at=master

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/d18cf8b811ce5996aacf88f417b26469d04c1bf6/Media/Documentation%20Pictures/B5.%20More%20Alternatives.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/d18cf8b811ce5996aacf88f417b26469d04c1bf6/Media/Documentation%20Pictures/B5.2%20Copy%20Api%20URL.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/d18cf8b811ce5996aacf88f417b26469d04c1bf6/Media/Documentation%20Pictures/B5.3%20Print.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/d18cf8b811ce5996aacf88f417b26469d04c1bf6/Media/Documentation%20Pictures/B5.4%20CSV.png
___

###Next step
####Related
