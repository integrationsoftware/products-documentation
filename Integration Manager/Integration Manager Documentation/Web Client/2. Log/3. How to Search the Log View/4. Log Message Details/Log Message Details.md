<tags data-value="Log,Events,Messages,View,Details"></tags>

Log Message Details
=====
					
[Done this? Next step](#next step)

___

##Information

Log Message Details shows several different kinds of information about the message and its travel.


___
##Instructions

To expand the details about the message, while viewing the log, click the button to the left of Action: 

![Details Button][1]

To open the details in a new tab, click View details, under Action

Opening Details in a new tab is necessary to [repair a message].

![New Tab][2]
___

The Information is divided into three parts: Additional Field Values, Context Values and Repository Model.

####Additional Field Values

Additional Field Values is the standard log information.

Below is an example with a logged temperature.

![Additional Field Values][3]

####Context Values

Context Values are usually custom values.

In this example, the city is Karlstad.

![Context Values][4]

####Repository Model

![Repository Model][5]

<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/5fb9628ca74a1c6d5889c6e339e1c2b55d680892/Media/Documentation%20Pictures/A12.%20Button.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/5fb9628ca74a1c6d5889c6e339e1c2b55d680892/Media/Documentation%20Pictures/A12.2%20View%20Details.png

[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/4f3869ef371b2ceb3d348df32839aae5958e4339/Media/Documentation%20Pictures/A13.%20Log%20Details.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/4f3869ef371b2ceb3d348df32839aae5958e4339/Media/Documentation%20Pictures/A13.2%20Context%20Values.png
[5]:https://bytebucket.org/integrationsoftware/products-documentation/raw/4f3869ef371b2ceb3d348df32839aae5958e4339/Media/Documentation%20Pictures/A13.3%20Repository%20Model.png

[repair a message]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/2.%20Log/4.%20How%20to%20Search%20the%20Log%20View/6.%20Repair%20Message/Repair%20Message.md?at=master
___

###Next step
####Related
