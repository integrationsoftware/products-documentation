<tags data-value="Search,Fields,Log,View"></tags>

Search Fields In Log View
=====
					
[Done this? Next step](#next step)

___

##Information

The first step is to [Search a Log View].

[Search Fields] uses [Mathematical Operators] and characters to filter the results of a [Log View].


##Instructions

###Log View with results

Here's a **Log View** without the use of Search Fields:

![Search Log][2]

###Mathematical Operator

Run Correlation Id Search Field without a value.

![Before][5]


Run Correlation Id Search Field with abc as value and Like as Operator.

This means that all events with abc somewhere in the Correlation Id within the chosen Time Range.

![Like][3]



###Results after Mathematical Operator

The platform shows the events within the search.

![Search Result][1]


###Matching Id

![Run Correlation iD][4]

___

###Group by Search Field

To group messages by Search Field, you first need to make sure that the Search Field is connected to the Log View.

Check: [Available Search Fields] and [Add or manage Log View].

####Select

Select the Search Field you want to group by.

![Select][9]

![Run Correlation Id][8]

	Tip: You can group and limit the search by characters at the same time

Hit Search.

![Search][6]

####Groups

Example of search result divided into three groups based on Run Correlation Id.

![Results][7]

Click Action to view [Log Message Details].

<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/10330054fa45a872e8b959a3fc360cd1d0fb06ab/Media/Documentation%20Pictures/A94.%20Search%20Log.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/10330054fa45a872e8b959a3fc360cd1d0fb06ab/Media/Documentation%20Pictures/A94.2%20Search%20Log%202.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/5ff33aa6376ac5655fb320be0b07b28f50f363e1/Media/Documentation%20Pictures/A94.3%20Like.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/5ff33aa6376ac5655fb320be0b07b28f50f363e1/Media/Documentation%20Pictures/A94.4%20Run%20Correlation%20Id.png
[5]:https://bytebucket.org/integrationsoftware/products-documentation/raw/4d4cdbc4e0420894901f5ded4992270a99f58f0d/Media/Documentation%20Pictures/A94.5%20Before%20value.png
[6]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9badc80cdab57ada8330ecefc0ad3c957bed54b6/Media/Documentation%20Pictures/B1.%20Group%20by%20Search%20Field.png
[7]:https://bytebucket.org/integrationsoftware/products-documentation/raw/b7d6d99c2bc6e2e78b15f1c763900faa6bfad5e7/Media/Documentation%20Pictures/B1.2%20Group%20by%20Search%20Field%20Result.png
[8]:https://bytebucket.org/integrationsoftware/products-documentation/raw/8a87d6c6b60205cb8bbedb53c6c27867bb1a9754/Media/Documentation%20Pictures/B1.3%20Group%20by%20Run%20Correlation%20Id.png
[9]:https://bytebucket.org/integrationsoftware/products-documentation/raw/932351a11d3a0cb9e92ff9bc347bb5a04b34a41f/Media/Documentation%20Pictures/B1.4%20Select.png

[Available Search Fields]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/2.%20Log/2.%20Create%20%20New%20Log%20View/3.%20Available%20Search%20Fields/Available%20Search%20Fields.md?at=master
[Log Message Details]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/2.%20Log/5.%20Log%20Message%20Details/Log%20Message%20Details.md?at=master
[Mathematical Operators]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/2.%20Log/6.%20Mathematical%20Search%20Operators/Mathematical%20Search%20Operators.md?at=master
[Search a Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/2.%20Log/4.%20How%20to%20Search%20the%20Log%20View/1.%20Search%20Log%20View%20and%20Download%20Content/Search%20Log%20View%20and%20Download%20Content.md?at=master
[Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/2.%20Log/Log.md?at=master
[Search Fields]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/5.%20Administration/1.%20Log/5.%20Search%20Fields/Search%20Fields.md?at=master
[Add or manage Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
___

###Next step
####Related
