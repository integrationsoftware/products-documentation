<tags data-value="Additional,Fields,Log,View,Directions"></tags>

Additional Fields In Log View
=====
					
[Done this? Next step](#next step)

___

##Information

The first step is to [Search a Log View].

Additional Fields include Log Text, Sequence Number, Log Status Code and Direction.

To be able to use the Additional Fields, make sure each field is selected in the edit view of the [Log View].

See [Add  or manage Log View].

##Instructions



###Additional Fields

Click the arrow to the right to show the dropdown with Additional Fields.

![Click][3]

![Additional Fields][2]



###Log Text

Log text is a piece of information coming with the message or event.

###Sequence Number

Sequence Number is a number labeled on the message or event.

###Log Status Code

Log Status Code is a number which tells us if the processing of the message went fine.

Log Status Code search uses [Mathematical Operators].

###Direction

The Directions to choose between is:

|	|
|---|
|All	|
|Receive	|
|Receive First	|
|Receive Last	|
|Send	|
|Send First	|
|Send Last	|
|Unknown	|

![Direction][1]



<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/888e67987d51bf9833c01dc98e71ecaa21f4022e/Media/Documentation%20Pictures/B2.%20Direction.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/888e67987d51bf9833c01dc98e71ecaa21f4022e/Media/Documentation%20Pictures/B2.2%20Additional%20Fields.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/69bcadef34fe5f3f6536949c8278c88ca7089e5f/Media/Documentation%20Pictures/B2.3%20Click%20Additional%20Fields.png


[Log Message Details]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/2.%20Log/5.%20Log%20Message%20Details/Log%20Message%20Details.md?at=master
[Mathematical Operators]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/2.%20Log/6.%20Mathematical%20Search%20Operators/Mathematical%20Search%20Operators.md?at=master
[Search a Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/2.%20Log/4.%20How%20to%20Search%20the%20Log%20View/1.%20Search%20Log%20View%20and%20Download%20Content/Search%20Log%20View%20and%20Download%20Content.md?at=master
[Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/2.%20Log/Log.md?at=master
[Search Fields]:https://bitbucket.org/integrationsoftware/products-documentation/src/ef08c79941c19f4f42cb242558a6dba6210f107d/Integration%20Manager/Integration%20Manager%20Documentation/5.%20Administration/1.%20Log/5.%20Search%20Fields/Search%20Fields.md?at=master
[Add or manage Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master

___

###Next step
####Related
