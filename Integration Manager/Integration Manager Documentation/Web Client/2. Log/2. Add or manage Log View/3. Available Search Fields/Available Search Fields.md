<tags data-value="Log,Search,Fields,"></tags>

Available Search Fields
=====
					
[Done this? Next step](#next step)

___

##Information

Choose which [Search Fields] should be available in the [Log View].

___
##Instructions

You can Add, Filter and Remove Search Fields.

![Search Fields][1]


<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/464fe94466e8de25b0aa725fef158a387f32200d/Media/Documentation%20Pictures/A17.%20Search%20Field.png

[Search Fields]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/5.%20Search%20Fields/Search%20Fields.md?at=master
[Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/Log.md?at=master

___

###Next step
####Related
