<tags data-value="Log,View,Create,New"></tags>

Add or manage Log View
=====
					
[Done this? Next step](#next step)

___

##Information
A [Log View] consists of events and messages of the included
 [Integrations], [Systems], [Services], [Message Types], [End Points], and [Log Agents].
 
There's also the option to choose which [Roles should be allowed] to view the **Log View**, which
[Restrictions] there should be, and which [Search Fields] should be available.

More information about [Roles].
___
##Instructions

### Name, Description and Website

A Name is required to create a **Log View**.

Adding a Description and a Website is optional. 

You will be able to search for the **Log View** by the text in the Description.

![Name, Description, and Website][1]

___

### Time Interval Configuration

You can add a Time Interval Configuration to the Log View.

![Time Interval Configuration][2]

The Time Interval Configuration consists of one or more time options.

For example, 42 minutes is an option.

![Last 42 Minutes][4]

####Allow Detailed Time Search

You can check Allow Detailed Time Search in the edit view of the Time Interval Configuration.

![Allow Detailed Time Search][6]

Then the users using the **Log View** will be able to Switch to Time Interval.

![Switch to Time Interval][5]



Information about [Time Interval Configurations].
 
Information about how to [Add or manage Time Interval Configurations].

___

### Advanced

#### Search by Sequence Number

Sequence Number is a number labeled on the message or event.

You have the option to allow search by Sequence Number which is helpful when you know what you are looking for.

___
#### Search by Log text 

Log text is a piece of information coming with the message or event. 

You have the option to allow search by Log text which is helpful when you know what you are looking for.

___
####Allow message to be Viewed in Source Format

Source Format is the content of the message or event before it was formatted.

It's helpful if your co-workers may have use of viewing the message as raw. 
___

#### Search Log Status Code

Log Status Code is a number which tells us if the processing of the message went fine.

For example, in BizTalk, the status code for OK is 0
___
####Allow users to resend and repair messages 

To [resend a message] may be useful when the message is fine but the receiver was faulty but is now fixed.

However, if you want to edit the message, it needs to be [repaired]. 


___

		Note: The advanced options can be unchecked if you want to ease up
	          the number of managable actions for your colleagues.



The Advanced options:

![Advanced][3]

<!--References -->

[Time Interval Configurations]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Time%20Interval%20Configurations/Time%20Interval%20Configurations.md?at=master
[Add or manage Time Interval Configurations]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Time%20Interval%20Configurations/2.%20Add%20Time%20Interval%20Configurations/Add%20Time%20Interval%20Configurations.md?at=master
[Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/Log.md?at=master

[Integrations]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/4.%20Included%20Integrations/Included%20Integrations.md?at=master
[Systems]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/5.%20Included%20Systems/Included%20Systems.md?at=master
[Services]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/6.%20Included%20Services/?at=master
[Message Types]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/7.%20Included%20Message%20Type/Included%20Message%20Type.md?at=master
[End Points]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/8.%20Included%20End%20Points/Included%20End%20Points.md?at=master
[Log Agents]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/9.%20Included%20Log%20Agents/Included%20Log%20Agents.md?at=master

[Roles should be allowed]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/1.%20Allow%20These%20Roles/Allow%20These%20Roles.md?at=master
[Restrictions]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/2.%20Restrictions/Restrictions.md?at=master
[Search Fields]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/3.%20Available%20Search%20Fields/Available%20Search%20Fields.md?at=master

[Roles]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[resend a message]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/4.%20How%20to%20Search%20the%20Log%20View/5.%20Resend%20Message/Resend%20Message.md?at=master
[repaired]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/4.%20How%20to%20Search%20the%20Log%20View/6.%20Repair%20Message/Repair%20Message.md?at=master

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/307e4c646abed9f789f4399bf75d3d174dd78432/Media/Documentation%20Pictures/8.%20Name%2C%20description%2C%20website.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/6d62616dc95f61166da83d36a939dbb4a7243c68/Media/Documentation%20Pictures/C13.%20Time%20Interval%20Configuration.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/307e4c646abed9f789f4399bf75d3d174dd78432/Media/Documentation%20Pictures/8.2%20Advanced.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/6d62616dc95f61166da83d36a939dbb4a7243c68/Media/Documentation%20Pictures/C13.2%2042%20Minutes.png
[5]:https://bytebucket.org/integrationsoftware/products-documentation/raw/6d62616dc95f61166da83d36a939dbb4a7243c68/Media/Documentation%20Pictures/C13.3%20Switch.png
[6]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9712b3f75dac5ea818a1ce49fc04077b35b93c40/Media/Documentation%20Pictures/C13.4%20Allow%20Detailed%20Time%20Search.png
___

###Next step
####Related
