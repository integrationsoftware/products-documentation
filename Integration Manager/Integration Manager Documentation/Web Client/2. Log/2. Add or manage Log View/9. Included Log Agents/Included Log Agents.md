<tags data-value="Included,Log,Agents"></tags>

Included Log Agents
=====
					
[Done this? Next step](#next step)

___

##Inforamtion

Choose which [Log Agents] to include in your [Log View].

If the Log Agents should be visible, click: [Visible In Search].

##Instructions

You can Add, Filter, Remove and Rename the Artifact.

![Include Integrations][1]


<!--References -->
[Visible In Search]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/3.%20Visible%20In%20Search/Visible%20in%20Search.md?at=master
[Log Agents]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/9.%20Included%20Log%20Agents/Included%20Log%20Agents.md?at=master
[Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/Log.md?at=master


[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/7cbb135d2df8f5ed41082b570912aab849f4efe0/Media/Documentation%20Pictures/A23.%20Log%20Agents.png
___

###Next step
####Related
