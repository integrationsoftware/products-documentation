<tags data-value="Roles,Log,Allow,Add,Edit"></tags>

Allow these Roles
=====
					
[Done this? Next step](#next step)

___

##Information

You can allow certain [Roles] to access a [Log View] in order to let the right people use it. 


___

##Instructions
Manage the Roles by Edit and then Add the Roles.


![Edit Roles][1]

You can also filter the available Roles to find the right one.

Remove Roles when they no longer need access to the **Log View**. 

![Allow Roles][2]


<!--References -->
[Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/Log.md?at=master

[Roles]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/71a6fc98bf082c0578471e5076b28e790d8637d1/Media/Documentation%20Pictures/10.%20Edit%20Allowed%20Roles.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/71a6fc98bf082c0578471e5076b28e790d8637d1/Media/Documentation%20Pictures/10.2%20Allow%20Roles%20in%20Log%20View.png
___

###Next step
####Related
