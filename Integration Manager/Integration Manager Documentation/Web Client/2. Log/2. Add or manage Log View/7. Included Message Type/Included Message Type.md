<tags data-value="Included,Message Type,Log"></tags>

Included Message Type
=====
					
[Done this? Next step](#next step)

___

##Information

Choose which [Message Types] to include in your [Log View].

If the Message Types should be visible, click: [Visible In Search].

##Instructions

You can add, filter, remove and rename the artifact.


![Include Integrations][1]


<!--References -->
[Visible In Search]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/3.%20Visible%20In%20Search/Visible%20in%20Search.md?at=master
[Message Types]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/7.%20Included%20Message%20Type/Included%20Message%20Type.md?at=master
[Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/Log.md?at=master


[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/7cbb135d2df8f5ed41082b570912aab849f4efe0/Media/Documentation%20Pictures/A21.%20Message%20Type.png
___

###Next step
####Related
