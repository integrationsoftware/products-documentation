<tags data-value="Included,Services,Log"></tags>

Included Services
=====
					
[Done this? Next step](#next step)

___

##Information

Choose which [Services] to include in your [Log View].

If the Services should be visible, click: [Visible In Search].

##Instructions

You can add, filter, remove and rename the artifact.


![Include Integrations][1]




<!--References -->
[Visible In Search]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/3.%20Visible%20In%20Search/Visible%20in%20Search.md?at=master
[Services]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/6.%20Included%20Services/?at=master
[Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/Log.md?at=master


[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/7cbb135d2df8f5ed41082b570912aab849f4efe0/Media/Documentation%20Pictures/A20.%20Services.png
___

###Next step
####Related
