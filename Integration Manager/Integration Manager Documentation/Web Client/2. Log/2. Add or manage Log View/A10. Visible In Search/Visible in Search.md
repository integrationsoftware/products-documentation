<tags data-value="Visible,In,Search"></tags>

Visible In Search
=====
					
[Done this? Next step](#next step)


___

##Information
If you want to show the [Integrations], [Systems], [Services], [Message Types], [End Points], and [Log Agents] in the [Log View], then use Visible In Search on each one of them.

You can do this when you create or edit a **Log View**. 

![Visible In Search][1]

<!--References -->

[Integrations]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/4.%20Included%20Integrations/Included%20Integrations.md?at=master
[Systems]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/5.%20Included%20Systems/Included%20Systems.md?at=master
[Services]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/6.%20Included%20Services/?at=master
[Message Types]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/7.%20Included%20Message%20Type/Included%20Message%20Type.md?at=master
[End Points]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/8.%20Included%20End%20Points/Included%20End%20Points.md?at=master
[Log Agents]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/9.%20Included%20Log%20Agents/Included%20Log%20Agents.md?at=master
[Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/Log.md?at=master

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/1340d20722fea80874cbd4b584aba6fac42a0813/Media/Documentation%20Pictures/A24.%20Visible%20In%20Search.png
___

###Next step
####Related
