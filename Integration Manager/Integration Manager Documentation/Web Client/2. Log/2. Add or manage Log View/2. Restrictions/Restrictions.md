<tags data-value="Restrictions,Log,Search Field,Expression,Operator"></tags>

Restrictions
=====
					
[Done this? Next step](#next step)

___

##Information

Restrictions is limiting the Search Field by using [Mathematical Operators], Expressions and Logical Operators.


##Instructions

###Search Restrictions

This is the Search Restriction window.

![Search Restrictions][1]

###Search Field

Choose a Search Field to Restrict.

Filter the result to easier find the right one.

![Search Field][2]

###Mathematical Operator

The Matematical Operator is the operation of the expression to generate a rule of which messages will be excluded.

![Mathematical Operator][3]

###Expression and Logical Operator

Expression is the value which together with the Mathematical Operator will decide what's excluded.

Logical Operator only works if at least two Restrictions are added and decides if both of them will be restricted or just one at a time.

Logical Operators won't work as within brackets.

![Expression and Logical Operator][4]



<!--References -->
[Visible In Search]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/3.%20Visible%20In%20Search/Visible%20in%20Search.md?at=master
[Mathematical Operators]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/6.%20Mathematical%20Search%20Operators/Mathematical%20Search%20Operators.md?at=master

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/1fe59481b726c19474bfa4fe1794a121ed857eff/Media/Documentation%20Pictures/A16.%20Restrictions.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/1fe59481b726c19474bfa4fe1794a121ed857eff/Media/Documentation%20Pictures/A16.%20Search%20Field.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/1fe59481b726c19474bfa4fe1794a121ed857eff/Media/Documentation%20Pictures/A16.%20Mathematical%20Operator.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/1fe59481b726c19474bfa4fe1794a121ed857eff/Media/Documentation%20Pictures/A16.%20Expression%20and%20logical%20operator.png
___
###Next step
####Related
