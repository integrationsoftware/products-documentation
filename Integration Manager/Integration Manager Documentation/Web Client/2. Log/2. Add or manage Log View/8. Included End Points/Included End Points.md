<tags data-value="Included,End Points,Log"></tags>

Included End Points
=====
					
[Done this? Next step](#next step)

___

##Information

Choose which [End Points] to include in your [Log View].

If the End Points should be visible, click: [Visible In Search].

##Instructions

You can add, filter, remove and rename the artifact.


![Include Integrations][1]


<!--References -->
[Visible In Search]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/3.%20Visible%20In%20Search/Visible%20in%20Search.md?at=master
[End Points]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/8.%20Included%20End%20Points/Included%20End%20Points.md?at=master
[Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/Log.md?at=master


[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/7cbb135d2df8f5ed41082b570912aab849f4efe0/Media/Documentation%20Pictures/A22.%20End%20Points.png
___

###Next step
####Related
