<tags data-value="Log,Events,Messages,View"></tags>

Log
=====
					
[Done this? Next step](#next step)

___

##Information

A **Log View** in Integration Manager consists of events and messages of the included
 [Integrations], [Systems], [Services], [Message Types], [End Points], and [Log Agents].

You can allow certain [Roles] to a **Log View** to make sure you aren't confusing your colleagues with too many **Logs View**.

You can also exclude [Search Fields] and make [Restrictions] to decrease the number of options.
___
The visible information includes:

|	|
|---|
|Log Date Time	|
|State	|
|Message Type	|
|End Point	|
|Direction	|
|Log text	| 

There's options to export all or selected messages to a .zip-file, Copy API URL, Print and Export to CSV.

Here's an example of a **Log View** in Integration Manager: 

![Log][1]

[How to add or manage a Log View].

<!--References -->

[Integrations]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/4.%20Included%20Integrations/Included%20Integrations.md?at=master
[Systems]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/5.%20Included%20Systems/Included%20Systems.md?at=master
[Services]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/6.%20Included%20Services/?at=master
[Message Types]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/7.%20Included%20Message%20Type/Included%20Message%20Type.md?at=master
[End Points]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/8.%20Included%20End%20Points/Included%20End%20Points.md?at=master
[Log Agents]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/9.%20Included%20Log%20Agents/Included%20Log%20Agents.md?at=master

[Roles]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Restrictions]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/2.%20Restrictions/Restrictions.md?at=master
[Search Fields]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/3.%20Available%20Search%20Fields/Available%20Search%20Fields.md?
[Add or manage Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/579eb0d095c706e9bc8ac139cc542f90ec1a8790/Media/Documentation%20Pictures/A11.%20Log.png
___

###Next step
####Related
