<tags data-value="Log,Views,Events,Messages,Search "></tags>

View All Log Views
=====
				
[Done this? Next step](#next step)

___

##Information
The Log View will give you information about your Logs
 such as Name, Description and Deleted Logs. 
 
 You can also Filter and Sort to find the right one more easily.

There are also options to edit, delete or clone a Log View. 
___
##Instructions

### Filter the result
You can filter the **Log Views** by typing in characters 
which is included in the name or the Description of the Log View.

![Filter][4]

###Sort
Sort the **Log Views** by Name or Description by clicking the words. 

The list is by default sorted from A-Z. 

![Sort][3]
### Edit, Delete or Clone
You'll find the options to edit, delete, or clone a Log View under Action.
####Edit
![Edit, Action and Clone][2]

Editing the Log View will take you to the editing page
 which is the same as the [Add or manage Log View] page.
 
####Include deleted

Delete will not delete the Log View but instead flag it as deleted.

When ["Include deleted"] is activated, all the "deleted" **Log Views** will be shown.

![Include Deleted][5]

####Clone
Clone will make a new copy of the Log View you choose.

It's useful when you want
to make a similar Log View without having to redo all the settings.

Give the cloned Log View a name and you'll get to the editing page to modify the settings.

![Clone Name][6]

Here's the Log View in its whole:

![View All **Log Views**][1]



<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/49af17ebdc4e2d5bb4cdc9ad8241358c21fa997c/Media/Documentation%20Pictures/7.%20View%20All%20Log%20Views.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/49af17ebdc4e2d5bb4cdc9ad8241358c21fa997c/Media/Documentation%20Pictures/7.2%20Edit.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/fe7ed13de91c662c34c31e33c393c22b7d8d0199/Media/Documentation%20Pictures/7.4%20Sort.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/fe7ed13de91c662c34c31e33c393c22b7d8d0199/Media/Documentation%20Pictures/7.3%20Filter.png
[5]:https://bytebucket.org/integrationsoftware/products-documentation/raw/cf9aea2a13574d84f759204fc336f7cc62d87251/Media/Documentation%20Pictures/7.5%20Include%20deleted.png
[6]:https://bytebucket.org/integrationsoftware/products-documentation/raw/adbffac45e445318bf2154642d0dff3295a6f1fe/Media/Documentation%20Pictures/7.6%20Clone.png

[Add or manage Log View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/2.%20Create%20%20New%20Log%20View/Create%20New%20Log%20View.md?at=master
["Include deleted"]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/8.%20Include%20Deleted/Include%20Deleted.md?at=master
___

###Next step
####Related
