<tags data-value="Status,IM,Service,Online,Views"></tags>

Status
=====
					
[Done this? Next step](#next step)

___

##Information

The Status button is always available in the upper right corner and will give you information about
the [Log] and the [Monitor].

![Status][1]

Available information is:

1. If the Integration Manager Logging Service is online.

2. If the Integration Manager Monitoring Service is online.

3. How many error views.

4. How many warning views.



![Log and Monitor][2]


<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A85.%20Status.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A85.2%20Log%20and%20Monitor.png

[Log]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/1.%20Log/Log.md?at=master
[Monitor]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/Monitor.md?at=master

___

###Next step
####Related
