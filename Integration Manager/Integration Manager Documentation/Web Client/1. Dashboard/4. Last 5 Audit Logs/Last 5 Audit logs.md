<tags data-value="Log,Monitor"></tags>

Last 5 Audit Logs
=====
					
[Done this? Next step](#next step)

___

##Information
The "Last 5 Audit Logs" gives you the last 5 events of the logged events by Integration Manager.

This can be helpful when you quickly want an overview over the 
last events to check that everything is as expected.

Although, since it's usually just a fraction of the events, it's only a quick monitoring tool.

___

Here's an example: 

![Last 5 Audit Logs][1]

<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/53035ee45cc8cde92d47fc90a39765d6038ab89a/Media/Documentation%20Pictures/2015-07-06%2008_00_16-Integration%20Manager%20-%20Dasboard.png
___

###Next step
####Related
