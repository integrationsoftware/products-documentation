<tags data-value="System,Errors"></tags>

System Errors This Week
=====
					
[Done this? Next step](#next step)

___

##Information
The "System Errors This Week" shows the number of error entries since Monday.

![System Errors This Week][1]

<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/ce22a91f3dea442fece498434e851719bd40be29/Media/Documentation%20Pictures/2.%20Number%20Of%20System%20Errors.png
___

###Next step
####Related
