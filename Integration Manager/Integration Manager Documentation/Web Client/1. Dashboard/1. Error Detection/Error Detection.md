<tags data-value="Error,Detection,Dashboard,Warnings"></tags>

Error detection
=====
					
[Done this? Next step](#next step)

___

##Information
The error detection field monitors the errors and warnings on the front page to keep you
 updated as soon as you enter the platform. 

You can see how many errors and warnings are detected and where they are located. 
The field is invisible when no issues are found.


Errors are prioritized over warnings which means that errors aren't showed when there's errors
 within either the log view or the monitor view.
 
___

####Either

This is what warnings and errors can look like:

![Warning][1]

![Error][2]

####Both

When the "Log View" have either errors or warnings and the "Monitor View" have the other, 
they will appear in seperate boxes:

  ![Warning and Error][3]

<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/650bd0e81330a262d59c90783dd19295d7ad766d/Media/Documentation%20Pictures/4.%20Dashboard%20Warning.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/650bd0e81330a262d59c90783dd19295d7ad766d/Media/Documentation%20Pictures/5.%20Dashboard%20Error.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/650bd0e81330a262d59c90783dd19295d7ad766d/Media/Documentation%20Pictures/6.%20Dashboard%20Warning%20And%20Error.png
___

###Next step
####Related
