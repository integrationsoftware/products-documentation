<tags data-value="Dashboard,Log,Monitor,Repository,Administration"></tags>

Dashboard
=====
					
[Done this? Next step](#next step)

___

##Information
The **Dashboard** is the starting point of Integration Manager. 

Here you will find the ["Error Detector"], 
"Administration Links", ["Last 5 Audit Logs"] and ["Number of System Errors this week"]. 

On the left is the links to the other main parts of the platform: [Log], [Monitor], [Repository] and [Administration].

___

Here's the **Dashboard**:

![System Errors This Week][1]

<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/d6c16d18d062f3977e083e58026df786cfff9e42/Media/Documentation%20Pictures/3.%20Dashboard.png

["Error Detector"]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/1.%20Dashboard/1.%20Error%20Detection/Error%20Detection.md?at=master
["Last 5 Audit Logs"]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/1.%20Dashboard/4.%20Last%205%20Audit%20Logs/Last%205%20Audit%20logs.md?at=master
["Number of System Errors this week"]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/1.%20Dashboard/5.%20System%20Errors%20This%20Week/System%20Errors%20This%20Week.md?at=master
[Log]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Monitor]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master
[Repository]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Administration]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/Administration.md?at=master

___

###Next step
####Related
