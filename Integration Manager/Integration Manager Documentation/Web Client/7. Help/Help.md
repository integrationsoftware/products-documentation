<tags data-value="Help,FAQ,Bug,Report,Feature"></tags>

Help
=====
					
[Done this? Next step](#next step)

___

##Information

The Help button is always available in the upper right corner and provides links to:

1. View Help in a new tab

2. Send Bug Report in a new tab

3. Send Feature Request in a new tab

4. View Information about Integration Manager.



![Help][1]

![General and More][2]

###Help and FAQ

![FAQ][3]

###Bug Report or Feature Request

![Bug Report or Feature Request][4]

###About

![About][5]

<!--References -->

[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A86.%20Help.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A86.2%20General%20and%20More.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A86.3%20FAQ.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A86.4%20Bug%20Report%20or%20Feature%20Request.png
[5]:https://bytebucket.org/integrationsoftware/products-documentation/raw/9a54c739d77bfddf050dd14f76601963f0bd711b/Media/Documentation%20Pictures/A86.5%20About.png

___

###Next step
####Related
