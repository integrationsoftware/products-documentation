<tags data-value="Information,About,Documentation,Structure"></tags>

Information about documentation
=====

### Structure

Each document in the documentation represents a "layer" of Integration Manager.

___

For example, you want information about Monitor Sources.

The way you reach Monitor Sources through the platform is Administration > Monitor > Monitor Views > Add Monitor View > Monitor Sources.

The documentation is built the same way and Monitor Sources will be found by the same path.
___

To see a full overview of all folders and documents, download this [Overview.txt] file.



<!--References -->

[Overview.txt]:https://bitbucket.org/integrationsoftware/products-documentation/raw/87b0aa40864a9ea37f21f4d8ed036a676490cf68/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/Overview.txt
