<tags data-value="Monitor,Web Service,Web Application,WCF,Web API"></tags>

Web Services Agent
=============================================

Monitor web services with Integration Manager's monitor agent for web services.
					
## About
Many systems and integrations are depending on web services, monitoring them is important! With Integration Manager’s agent for web services you can easily monitor web API’s, SOAP services and even web sites. 

You monitor them by simply providing all required HTTP headers, client certificates as well as the services’ address and let the agent do its job – connecting to the service, check if the response’s HTTP status is one of the expected ones and evaluate the result of the request by using a regular expression or an XPath. 

Not only can you check if the HTTP status is one of the expected ones, and not only evaluate the result by using a regular expression or an XPath, you can even monitor the response time of the request.

Technical Features

* SOAP
* Web API
* Web Applications
* Body from file 
* All HTTP operations are supported
* All HTTP headers are supported
* Validate response body using RegEx or XPath
* Validate HTTP response status code
* Authentication support
	* Client Cert
	* Impersonation
	* Basic authentication
	* API Key

## Monitor Capabilities
List of resources that can be monitored by using this agent

* Any HTTP endpoint can be monitored
* Any number of web end points can be monitored from a single agent
* Multiple agents can be deployed

## Actions
No actions have yet been implemented.