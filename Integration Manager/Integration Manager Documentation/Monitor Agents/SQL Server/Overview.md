<tags data-value="Monitor,Agent,SQL Server,SQL,Database"></tags>

SQL Server Agent
=============================================

Monitor SQL Server with Integration Manager's monitor agent for SQL Server.
					
## About
It is crucial to know when backups are not taken, jobs are not running and statements are not returning the expected result, due to the fact that this might cause business critical errors. 
With this agent you can configure Integration Manager to monitor all imaginable SQL statements and their result.

Monitor SQL jobs on all configured SQL server instances. 

Control that backups have been taken from all monitored databases. Displays the time and state of the last backup (if any).

Check the size of the files for data and log, supports warning and error threshold values.'

* Any number of SQL Instances can be monitored from a single agent
* Multiple agents can be deployed

## Monitor Capabilities
List of resources that can be monitored by using this agent

* SQL jobs
* Backups
* Size checks (data and log separately).
* Custom SQL statements

## Actions
No actions have yet been implemented.