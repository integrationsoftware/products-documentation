<tags data-value="Monitor,Windows,Services"></tags>

Windows Services Agent
=============================================

Monitor Windows services with Integration Manager's monitor agent for Windows services.
					
## About
This agent allows you to monitor the state of all Windows services on defined machines. In the configuration files you point out on which servers you would like to monitor the installed Windows services and Integration Manager will then automatically get a list of all Windows services. This allows you to simply monitor any Windows service on any available machine!

* Any number of Windows Servers can be monitored from a single agent
* Multiple agents can be deployed on multiple servers for scalability, security and performance
* Filter can be set to include/exclude services depending on startup mode, Automatic and Automatic (Delayed) are always included
	* Disabled services – default excluded
	* Manual Services – default excluded
* Server can have a friendly name (use IP or ServerName for connection)
* Ping before connect option when monitoring a large number of servers
	* By enabling ping there will be a shorter timeout if targeted server is unavailable due to being shutdown or rebooting. Option is by default disabled.


## Monitor Capabilities
List of resources that can be monitored by using this agent

* Windows Services

## Actions
Integration Manager Web Client can send Actions to the Integration Manager's monitor agent for Windows services requesting operations to be performed on the monitored resources. With the existing privilege model you can allow certain users to perform operation on selected resources.

List of actions that can be executed using this agent

* Start/Stop Windows service
* View details of the Windows services
