<tags data-value="Monitor,Windows,Folders,Files"></tags>

Folders Agent
=============================================

Monitor folders with Integration Manager's monitor agent for folders.
					
## About
Monitoring folders is not an easy task to do. Folder monitor agent enables you to monitor files within defined folders. Allowing you to set up a configuration file in which you describe the folder(s) you would like to monitor and how much time the files can spend being in that folder. If difference between the current time and the time the file has been created within the folder is larger than the time span defined in the configuration file, Integration Manager will then mark this as an error.

## Monitor Capabilities
List of resources that can be monitored by using this agent

* Any number of file shares can be monitored from a single agent
* Multiple agents can be deployed
* File Age verification per folder with options:
	* Root only
	* Root and child folders

## Actions
No actions have yet been implemented.