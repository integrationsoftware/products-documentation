<tags data-value="Monitor,BizTalk,Control Center"></tags>

Microsoft BizTalk Server Agent
=============================================

Monitor your BizTalk environment with Integration Manager's monitor agent for [Microsoft BizTalk Server](http://www.microsoft.com/en-us/server-cloud/products/biztalk/default.aspx).
					
## About
Integration Manager’s agent for [Microsoft BizTalk Server](http://www.microsoft.com/en-us/server-cloud/products/biztalk/default.aspx) enables you to control and monitor BizTalk resources. The agent must be installed with one instance per BizTalk Group. The agent should not be clustered due to local configuration files. The agent can be clustered if the settings file are updated on all cluster servers.

In order to monitor a BizTalk group the agent must be installed on a server with BizTalk Administration Console and that has local network access to BizTalk's SQL database (management and messagebox). The BizTalk administration console version must be the same or higher compared to the BizTalk group being controlled and monitored.

## Monitor Capabilities
List of [Microsoft BizTalk Server](http://www.microsoft.com/en-us/server-cloud/products/biztalk/default.aspx) resources that can be monitored by using this agent

* Host instances (cluster support)
	* Tracking host configuration and state (make sure at least one per server)
* Send Ports
* Receive Locations
* Orchestrations
* Send Port Groups
* Suspended instance(s) (resumable)
* Suspended instance(s) (not resumable)
* Scheduled instance(s)
* Dehydrated orchestration instance(s)
* Active instance(s)
* Ready to run instance(s)

## Actions
Integration Manager Web Client can send Actions to the Integration Manager's monitor agent for BizTalk Server requesting operations to be performed on the monitored resources. With the existing privilege model you can allow certain users to perform operation on selected resources.

List of actions that can be executed using this agent

* Start/Stop host instances
* Start/Stop Receive Locations
* Start/Stop/Enlist/Unenlist Send Ports
* Start/Stop/Enlist/Unenlist Orchestrations
* Resume suspended instances per application
* Terminate suspended instances per application
* View suspended messages (first 1000 bytes)

Since BizTalk can get arbitrary number of suspended messages there is a user-defined limit on the maximum number of suspended messages to retrieve in the Action ‘View suspended messages’.