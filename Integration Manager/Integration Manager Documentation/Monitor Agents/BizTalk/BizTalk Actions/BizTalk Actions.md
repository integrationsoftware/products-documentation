<tags data-value="BizTalk,Actions,Monitor,View,Resource"></tags>

BizTalk Actions
=====
					
[Done this? Next step](#next step)

___

##Information

While [Viewing a Single Monitor View], you can do [Actions] to [Resources].

These Actions differ which depends if it's a [BizTalk] or [Windows Service] Resource.

|Available BizTalk Actions:	|	
|---|
|[List suspended instances] 	|	
|[Resume suspended instances]	|
|[Stop Recieve Location] 	|	
|[Terminate suspended instances]	|	
	

### Actions



|Available Actions:	|	
|---|
|Start	|	
|Stop	|
|Enlist	|	
|Unenlist	|	
|Details	|	

![All Actions][1]

###Some Actions

|Available Actions:	|	
|---|
|Start	|	
|Stop	|	
|Details	|


![Some Actions][3]

###No Actions

Sometimes, no Actions are available.

![No Actions][2]



###Details

Details of a Biztalk Resource.

![Details][8]

###List suspended instances


|Available Actions:	|	
|---|
|List suspended instances	|	
|Terminate all	|	
|Resume All	|

![List suspended instances][9]



<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/0b723d16c0f518560b8c6924a055b03cb01f4c72/Media/Documentation%20Pictures/C15.%20All%20Actions.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/0b723d16c0f518560b8c6924a055b03cb01f4c72/Media/Documentation%20Pictures/C15.2%20No%20Actions.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/0b723d16c0f518560b8c6924a055b03cb01f4c72/Media/Documentation%20Pictures/C15.3%20Some%20Actions.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/637a7004a599b79f550d0ff686a258ac5dc1bcd9/Media/Documentation%20Pictures/C15.3%20Some%20Actions.png

[5]:https://bytebucket.org/integrationsoftware/products-documentation/raw/48dc04c3f0ea9b0dde3ed1696e05c66fa3e42c17/Media/Documentation%20Pictures/A27.%20Live%20Overview.png
[6]:https://bytebucket.org/integrationsoftware/products-documentation/raw/663df5fd5a6867c63dc3eee768e508077a636e3c/Media/Documentation%20Pictures/A27.3%20Single%20Monitor.png
[7]:https://bytebucket.org/integrationsoftware/products-documentation/raw/c90d29249fae13f74c1f8d52f08a395e914eef97/Media/Documentation%20Pictures/A27.4%20Customize%20Historic%20Events.png

[8]:https://bytebucket.org/integrationsoftware/products-documentation/raw/a39bc7d56d42374ce761720b06525d62d4c7bde0/Media/Documentation%20Pictures/C18%20Details.png
[9]:https://bytebucket.org/integrationsoftware/products-documentation/raw/0b723d16c0f518560b8c6924a055b03cb01f4c72/Media/Documentation%20Pictures/C16.%20Action%20List%20suspended%20instances.png

[Monitor View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/3.%20Monitor/Monitor.md?at=master
[BizTalk]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Monitor%20Agents/BizTalk/BizTalk.md?at=master
[Actions]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/Actions/Actions.md?at=master

[List suspended instances]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Monitor%20Agents/BizTalk/Actions/1.%20List%20suspended%20instances/List%20suspended%20instances.md?at=master
[Resume suspended instances]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Monitor%20Agents/BizTalk/Actions/2.%20Resume%20suspended%20instances/Resume%20suspended%20instances.md?at=master
[Stop Recieve Location]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Monitor%20Agents/BizTalk/Actions/3.%20Stop%20Receive%20Location/Stop%20Receive%20Location.md?at=master	
[Terminate suspended instances]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Monitor%20Agents/BizTalk/Actions/4.%20Terminate%20suspended%20instances/Terminate%20suspended%20instances.md?at=master

[Viewing a Single Monitor View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/View%20Single%20Monitor%20View.md?at=master
[Windows Service]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Monitor%20Agents/Windows%20Services/Windows%20Services.md?at=master
[Resources]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
___

###Next step
####Related
