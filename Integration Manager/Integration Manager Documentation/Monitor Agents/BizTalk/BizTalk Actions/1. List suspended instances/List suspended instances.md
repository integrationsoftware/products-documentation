<tags data-value="Monitor,Views,Live,Health,Graph"></tags>

List suspended instances
=====
					
[Done this? Next step](#next step)

___

##Information





[View Single Monitor View]



___

##Instructions


![Suspended Instances][4]

![Actions][1]

![List Of Suspended Details][2]

![Details][3]






<!--References -->
[1]:https://bytebucket.org/integrationsoftware/products-documentation/raw/0b723d16c0f518560b8c6924a055b03cb01f4c72/Media/Documentation%20Pictures/C16.%20Action%20List%20suspended%20instances.png
[2]:https://bytebucket.org/integrationsoftware/products-documentation/raw/0b723d16c0f518560b8c6924a055b03cb01f4c72/Media/Documentation%20Pictures/C16.2%20List%20of%20suspended%20instances.png
[3]:https://bytebucket.org/integrationsoftware/products-documentation/raw/0b723d16c0f518560b8c6924a055b03cb01f4c72/Media/Documentation%20Pictures/C16.3%20Details.png
[4]:https://bytebucket.org/integrationsoftware/products-documentation/raw/6bb6f4873ed15e9ef819c3eae6ca72655a900f70/Media/Documentation%20Pictures/C16.4%20Suspended%20Instances.png

[Monitor]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master
[View Single Monitor View]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/View%20Single%20Monitor%20View.md?at=master

___

###Next step
####Related
