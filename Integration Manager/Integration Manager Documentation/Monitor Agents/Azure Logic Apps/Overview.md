<tags data-value="Monitor,Azure,App Services,Logic App"></tags>

Microsoft Azure Logic Apps
=============================================

Monitor [Microsoft Azure App Service - Logic Apps][1] with Integration Manager's monitor agent for [Microsoft Azure App Service - Logic Apps][1].
					
## About
This agent allows you to monitor [Microsoft Azure App Service - Logic Apps][1] by using Integration Manager.

## Monitor Capabilities
List of resources that can be monitored by using this agent

* Monitor state of each available logic app
* Log messages from all available logic apps
* Automatically adds API apps used in the logic apps to be monitored.

## Actions
No actions have yet been implemented.

[1]: https://azure.microsoft.com/en-us/services/app-service/logic/        "Microsoft Azure App Service - Logic Apps"