<tags data-value="Monitor,Windows,MSMQ,Queue"></tags>

MSMQ Agent
=============================================

Monitor MSMQ with Integration Manager's monitor agent for MSMQ.
					
## About
This agent allows you to monitor MSMQ queues. Checks can be performed on the allowed number of messages within them. Also the maximum age of the first message on the queue can be verified. You decide in the configuration file which server and queue it is you want to monitor, as well as the number of messages on which Integration Manager should warn / fail for. 

This service has been developed due to the fact that in many cases, that if queues contain messages it is in many cases some kind of error, at least if the messages are there longer than an expected time span (e.g. 2 hours). This agent enables you to monitor each and every queue that is important for you and your business.

## Monitor Capabilities
List of resources that can be monitored by using this agent

* Public queues support
* Private queues support
* Dead letter queues support
* Age verification
* Count (warning / error)
* Per queue setting can be overridden from default values

## Actions
No actions have yet been implemented.