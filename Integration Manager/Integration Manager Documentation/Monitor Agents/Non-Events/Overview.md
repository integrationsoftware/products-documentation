<tags data-value="Monitor,Windows,Non Events,BizTalk, Mule,System Integration,Azure Logic Apps"></tags>

Non-Events Agent for Integration Manager
=============================================
Monitor agent for Integration Manager that monitors non-events. Make sure you get the alert before your customer notices a problem. Improves the experience, quality and credibility of your system integration service. Enables your organization support and maintain integrations according to SLA commitments. Acts as a foundation for charging models. You cannot have SLA commitments without proper tooling like this non-event agent.  
					
## About
This agent allows you to monitor Non-Events on logged data by re-using Integration Manager's security restricted log views for business, IT-ops and other stake holders. This agent is easy to use; Simply copy the API link provided in the log view(s) and define the timespan to evaluate. This agent can be used on data logged from any platform over any selected timespan due to our superior long time logging support.

## Monitor Capabilities
* Individual settings for Exceptions and Warnings dependent on customizable thresholds
* Evaluates the actual number of logged events
    * Min - Trigger an alert if not at least number of messages have been executed
    * Max - Trigger an alert if more than expected number of messages have been executed
* Evaluate any message type from any/all endpoint(s)
* Evalute Ack/Nak
    * Correlates on any message type (technical/functional)
    * Correlate and match using data from payload/context
    * Timespan according to your configuration
    * Makes sure you stay within agreed SLA levels
        * Trigger an alert if outside boundaries
    * Alert information provides relevant information     
* Smart filtering
    * Exclude vacation and or non-business dates
    * Supports user defined business hours
    * Content-based filtering.

### Examples
* Number of orders received in the last 30 days must be bigger than 500, but not bigger than 1500
* Ack/Nak - Send alert if response has not been logged in given time interval.
* Number of accepted orders from customer A on mondays between 8:03 to 13:37 must be less than 500.
* Number of accepted orders from customer A on mondays between 8:03 to wednesday 13:37 must be less than 500.
* Send warning alert if number of orders and invoices to customer A and B is too low, excluded dates: 17/5, 6/7 and 24/12.
* Send exception alert if response (Ack/Nak) has not been received in the given time interval.

## Actions
Actions for this agent have yet not been implemented. Please feel free to request user feedback on possible actions for us to implement. 

## Pricing
This agent and all other Monitor agents are part of the license for Integration Manager. No additional costs exists for the use of the non-event Monitor Agent for Integration Manager licencees. 