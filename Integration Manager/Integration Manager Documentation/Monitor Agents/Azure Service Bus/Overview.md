<tags data-value="Monitor,Azure,Service Bus,Queue"></tags>

Microsoft Azure Service Bus Agent
=============================================

Monitor [Microsoft Azure Service Bus](https://azure.microsoft.com/en-us/services/service-bus/) with Integration Manager's monitor agent for [Microsoft Azure Service Bus](https://azure.microsoft.com/en-us/services/service-bus/).
					
## About
This agent allows you to monitor Microsoft Azure Service Bus queues & topics. Checks can be performed on the allowed number of messages within them. Also the maximum age of the first message on the queue can be verified. You decide in the configuration file which server and queue it is you want to monitor, as well as the number of messages on which Integration Manager should warn / fail for. 

This service has been developed due to the fact that in many cases, that if queues contain messages it is in many cases some kind of error, at least if the messages are there longer than an expected time span (e.g. 2 hours). This agent enables you to monitor each and every queue that is important for you and your business.

## Monitor Capabilities
List of resources that can be monitored by using this agent

* [Service Bus Queues](https://azure.microsoft.com/en-us/documentation/articles/service-bus-fundamentals-hybrid-solutions/#queues)
* [Service Topics](https://azure.microsoft.com/en-us/documentation/articles/service-bus-fundamentals-hybrid-solutions/#topics)
* [Service Bus Relay](https://azure.microsoft.com/en-us/documentation/articles/service-bus-fundamentals-hybrid-solutions/#relays)
* Dead letter 
* Age verification
* Count (warning / error)
* Per queue setting can be overridden from default values

## Actions
No actions have yet been implemented.