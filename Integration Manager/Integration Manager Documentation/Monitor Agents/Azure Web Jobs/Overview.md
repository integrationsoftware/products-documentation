<tags data-value="Monitor,Azure,Web Jobs,App Services"></tags>

Microsoft Azure Web Jobs
=============================================

Monitor [Microsoft Azure Web Jobs](https://azure.microsoft.com/sv-se/documentation/articles/web-sites-create-web-jobs/) with Integration Manager's monitor agent for [Microsoft Azure Web Jobs](https://azure.microsoft.com/sv-se/documentation/articles/web-sites-create-web-jobs/).

## About
This agent allows you to monitor Microsoft Web Jobs.

WebJobs is a complete compute solution on a managed platform. The runtime offers many choices on how your code can run (cron, service, manually). Using Azure Websites as a platform means that WebJobs inherits tons of cool features. Azure Scheduler brings a plethora of scheduling choices. The ASP.NET team built the WebJobs SDK, which makes writing code that interfaces with the different Azure components (Blob, Queues, Tables, Service Bus) a snap! And the rich Visual Studio tooling wraps everything together seamlessly. Goes to show that sometimes the whole is bigger than the sum of its parts.

## Monitor Capabilities
List of resources that can be monitored by using this agent

* Web Jobs State (running, stopped)
* Recent job (whether the last job has succeeded)

## Actions

* Start / Stop web jobs
* Run web job
* View details
