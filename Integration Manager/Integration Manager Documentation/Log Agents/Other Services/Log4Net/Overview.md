<tags data-value="Monitor,Azure,App Services,Logic App"></tags>

Microsoft Azure App Services
=============================================

Monitor [Microsoft Azure App Services](https://azure.microsoft.com/en-us/services/app-service) with Integration Manager's monitor agent for [Microsoft Azure App Services](https://azure.microsoft.com/en-us/services/app-service).

## About
This agent allows you to monitor Microsoft Azure App Services by using Integration Manager.

## Monitor Capabilities
List of resources that can be monitored by using this agent

* Logic Apps Support
	* Monitor state of each available logic app
	* Log messages from all available logic apps
	* Automatically adds API apps used in the logic apps to be monitored.

## Actions
No actions have yet been implemented.
